# README #
This is my 40%project.  

Update Project Manager WS

this is a webservice that has two interfaces:
 1.  GUI interface, so that user can enter parameter via a 
 form.  The resulting report will be display on a web page.
 2.  parameters are enter via  json structure and a response is
 returned with a json structure (see https://docs.google.com/document/d/15IdvVYW7CunFPZqQ2r_QjCbWhXee0Htmr-uZkI2Rb5k/edit
  )
 


Function:
The code will update the program manager fields in the database table for FD,SP and AP
It will also update the UDFs for project





### How do I get set up? ###

* Summary of set up
to run this webservice:
from directory that code resides (users/rreiss/git/40percent)/40percent-updatepm
>pip install virtualenv
>virtualenv venv
>venv\Scripts\activate
>python manage.py runserver

to get GUI displayed :
URL:  localhost:8000/itssupp

to use json interfaces:
post json to http:localhost:8000/itssupp/update-pm/

* Run



* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Becky
