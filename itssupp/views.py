from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.views import Response
from rest_framework.views import status

import json
import copy

from itssupp.controllers.Support_updatePM import Support_updatePM
from itssupp.controllers.Support_abandonFDorSPandChildren import Support_abandonFDorSPandChildren
from itssupp.controllers.Support_deleteFDandChildren import Support_deleteFDandChildren
from itssupp.controllers.Support_deleteSPandChildren import Support_deleteSPandChildren
from itssupp.controllers.Support_changeAccountInfo import Support_changeAccountInfo


#this class is created for REST API using Json get (does nothing) and post
class update_PM(APIView):
    def get(self,request):
        pass


    def post(self,request):
        body = request.data  # holds the json request
        #print (type(body))
        print (body)
        #get data from the request
        submittedBy = body['submitted-by']
        pmid = str (body['pm-id'])
        fdOrSpids = body['fdORsp-ids']
        # -----
        server = 'Int'
        jiraTicket = 'xxxx'
        func = 'UpdatePM'
        responsejSon = copy.deepcopy(body)



        mySupportTask = Support_updatePM()
        mySupportTask.doUpdatePM(server,"", func, fdOrSpids, pmid,submittedBy)  # get arguments from request.data
        responsejSon["pm-name"] =mySupportTask.projectManagerName
        return Response({"message": "Updated PM for FDs and Children!", "data": responsejSon})




class projectManagerGui:

    def index(request):
        # template = loader.get_template('itssupp/index.html')
        return render(request, 'itssupp/index.html')


    def gui_UpdatePMpage(request):
        return render(request, 'itssupp/gui_UpdatePMpage.html')


    # -------------------------------------------------------------------------------
    # UPDATE PROGRAM MANAGER
    # this gets all the data from the input form and sends to changeProgramManagerPreport.html
    def infoUpdatePM(request):
        mySupportTask = Support_updatePM()
        form = request.GET  # get the input from the form on the page
        servername = form['server']
        ticketnum = form['ticketnum']
        function = form['function']
        spOrFd = form['SpsOrFds']
        pmID = form['PMid']

        mySupportTask.doUpdatePM(servername, ticketnum, function, spOrFd, pmID, submittedBy=5981) #(server,jiraTicket, func, fdOrSpids, pmid,submittedBy)
        NumErrors = str(mySupportTask.errorCnt)
        taskServer = mySupportTask.server
        taskDBcomment = mySupportTask.sComment
        taskFunction = mySupportTask.functionText
        spSet = mySupportTask.spSet
        apSet = mySupportTask.apSet
        fdSet = mySupportTask.fdSet
        confirmQuery = mySupportTask.confirmQuery
        taskServerName = mySupportTask.servername
        projManagerName = mySupportTask.projectManagerName
        projURLset = mySupportTask.urlSet

        return render(request, 'itssupp/reportUpdatePM.html', {'server': taskServer,
                                                               'comment': taskDBcomment,
                                                               'errors': mySupportTask.errorCnt,
                                                               'errormsg': mySupportTask.errorMessage,
                                                               'function': taskFunction,
                                                               'sps': spSet,
                                                               'aps': apSet,
                                                               'fds': fdSet,
                                                               'pmID': pmID,
                                                               'query': confirmQuery,
                                                               'jiraTicket': ticketnum,
                                                               'serverName': taskServerName,
                                                               'manager': projManagerName,
                                                               'urls': projURLset})

class abandonFDSPGui:
    def gui_abandonPage(request):
        return render(request, 'itssupp/gui_abandonPage.html')

    # this gets all the data from the input form and sends to reportAbandonSP.html or reportAbandonFD.html
    def infoAbandon(request):
        mySupportTask = Support_abandonFDorSPandChildren()
        form = request.GET  # get the input from the form on the page
        servername = form['server']
        ticketnum = form['ticketnum']
        function = form['function']
        spOrFd = form['SpsOrFds']
        aps = form['Aps']

        mySupportTask.doAbandon(servername, ticketnum, function, spOrFd, aps)
        NumErrors = str(mySupportTask.errorCnt)
        taskServer = mySupportTask.server
        taskDBcomment = mySupportTask.sComment
        taskFunction = mySupportTask.functionText
        sampleSet = mySupportTask.sampleSet
        sowSet = mySupportTask.sowSet
        spSet = mySupportTask.spSet
        confirmQuery = mySupportTask.confirmQuery
        workflowURL = mySupportTask.route2workflowURL
        taskServerName = mySupportTask.servername

        if (function == "SPchildren" or function == "SPchildrenTest"):
            return render(request, 'itssupp/reportAbandonSP.html', {'server': taskServer,
                                                                    'comment': taskDBcomment,
                                                                    'errors': mySupportTask.errorCnt,
                                                                    'errormsg': mySupportTask.errorMessage,
                                                                    'function': taskFunction,
                                                                    'samples': sampleSet,
                                                                    'sows': sowSet,
                                                                    'sps': spSet,
                                                                    'query': confirmQuery,
                                                                    'url': workflowURL,
                                                                    'jiraTicket': ticketnum,
                                                                    'serverName': taskServerName})
        elif (
                function == "FDchildren" or function == "FDchildrenTest"):  # (function == "FDchildren" or function == "FDchildrenTest"):
            apSet = mySupportTask.apSet
            atSet = mySupportTask.atSet
            fdSet = mySupportTask.fdSet
            return render(request, 'itssupp/reportAbandonFD.html', {'server': taskServer,
                                                                    'comment': taskDBcomment,
                                                                    'errors': mySupportTask.errorCnt,
                                                                    'errormsg': mySupportTask.errorMessage,
                                                                    'function': taskFunction,
                                                                    'samples': sampleSet,
                                                                    'sows': sowSet,
                                                                    'sps': spSet,
                                                                    'aps': apSet,
                                                                    'ats': atSet,
                                                                    'fds': fdSet,
                                                                    'query': confirmQuery,
                                                                    'url': workflowURL,
                                                                    'jiraTicket': ticketnum,
                                                                    'serverName': taskServerName})
        else:  # (function == "APandSPchildren" or function == "APandSPchildrenTest")
            apSet = mySupportTask.apSet
            atSet = mySupportTask.atSet
            return render(request, 'itssupp/reportAbandonAPandSP.html', {'server': taskServer,
                                                                         'comment': taskDBcomment,
                                                                         'errors': mySupportTask.errorCnt,
                                                                         'errormsg': mySupportTask.errorMessage,
                                                                         'function': taskFunction,
                                                                         'samples': sampleSet,
                                                                         'sows': sowSet,
                                                                         'sps': spSet,
                                                                         'aps': apSet,
                                                                         'ats': atSet,
                                                                         'query': confirmQuery,
                                                                         'url': workflowURL,
                                                                         'jiraTicket': ticketnum,
                                                                         'serverName': taskServerName})


#
class deleteFDGui:
    def gui_deleteFDPage(request):
        return render(request, 'itssupp/gui_deleteFDPage.html')

    # this gets all the data from the input form and sends to reportAbandonSP.html or reportAbandonFD.html
    def infoDeleteFD(request):
        mySupportTask = Support_deleteFDandChildren()
        form = request.GET  # get the input from the form on the page
        servername = form['server']
        ticketnum = form['ticketnum']
        function = form['function']
        spOrFd = form['SpsOrFds']


        mySupportTask.doDeleteFD(servername, ticketnum, function, spOrFd)
        NumErrors = str(mySupportTask.errorCnt)
        taskServer = mySupportTask.server
        taskDBcomment = mySupportTask.sComment
        taskFunction = mySupportTask.functionText
        sampleSet = mySupportTask.sampleSet
        sowSet = mySupportTask.sowSet
        spSet = mySupportTask.spSet
        confirmQuery = mySupportTask.confirmQuery
        workflowURL = mySupportTask.route2workflowURL
        taskServerName = mySupportTask.servername

        if  (function == "FDchildren" or function == "FDchildrenTest"):  # (function == "FDchildren" or function == "FDchildrenTest"):
            apSet = mySupportTask.apSet
            atSet = mySupportTask.atSet
            fdSet = mySupportTask.fdSet
            return render(request, 'itssupp/reportDeleteFD.html', {'server': taskServer,
                                                                    'comment': taskDBcomment,
                                                                    'errors': mySupportTask.errorCnt,
                                                                    'errormsg': mySupportTask.errorMessage,
                                                                    'function': taskFunction,
                                                                    'samples': sampleSet,
                                                                    'sows': sowSet,
                                                                    'sps': spSet,
                                                                    'aps': apSet,
                                                                    'ats': atSet,
                                                                    'fds': fdSet,
                                                                    'query': confirmQuery,
                                                                    'url': workflowURL,
                                                                    'jiraTicket': ticketnum,
                                                                    'serverName': taskServerName})



#
class deleteSPGui:
    def gui_deletePage(request):
        return render(request, 'itssupp/gui_deletePage.html')

    # this gets all the data from the input form and sends to reportDeleteSP.html o
    def infoDelete(request):
        mySupportTask = Support_deleteSPandChildren()
        form = request.GET  # get the input from the form on the page
        servername = form['server']
        ticketnum = form['ticketnum']
        function = form['function']
        spOrFd = form['SpsOrFds']


        mySupportTask.doDelete(servername, ticketnum, function, spOrFd)
        NumErrors = str(mySupportTask.errorCnt)
        taskServer = mySupportTask.server
        taskDBcomment = mySupportTask.sComment
        taskFunction = mySupportTask.functionText
        sampleSet = mySupportTask.sampleSet
        sowSet = mySupportTask.sowSet
        spSet = mySupportTask.spSet
        confirmQuery = mySupportTask.confirmQuery
        workflowURL = mySupportTask.route2workflowURL
        taskServerName = mySupportTask.servername

        if (function == "SPchildren" or function == "SPchildrenTest"):
            return render(request, 'itssupp/reportDeleteSP.html', {'server': taskServer,
                                                                    'comment': taskDBcomment,
                                                                    'errors': mySupportTask.errorCnt,
                                                                    'errormsg': mySupportTask.errorMessage,
                                                                    'function': taskFunction,
                                                                    'samples': sampleSet,
                                                                    'sows': sowSet,
                                                                    'sps': spSet,
                                                                    'query': confirmQuery,
                                                                    'url': workflowURL,
                                                                    'jiraTicket': ticketnum,
                                                                    'serverName': taskServerName})



#
class changeAccountGui:
    def gui_changeAccountPage(request):
        return render(request, 'itssupp/gui_changeAccountPage.html')

    # Change Account Info
    # this gets all the data from the input form and sends to changeAccountInfoMOre
    def infoChangeAccount(request):
        mySupportTask = Support_changeAccountInfo()
        form = request.GET  # get the input from the form on the page
        servername = form['server1']
        ticketnum = form['ticketnum']
        function = form['function']
        spOrFd = form['SpsOrFds']
        acctID = form['acctID']
        sciPrg = form['sciPrg']
        purpose = form['purpose']
        userPrg = form['userPrg']
        year = form['year']

        # process the input
        mySupportTask.processInput(servername, function, ticketnum, spOrFd, acctID, sciPrg, purpose, userPrg, year)
        # set up for output

        # sends data for output page
        return render(request, 'itssupp/reportChangeAcctInfo.html', {'server': mySupportTask.servername,
                                                                     'function': mySupportTask.functionText,
                                                                     'jiraTicket': ticketnum,
                                                                     'errors': mySupportTask.errorCnt,
                                                                     'errormsg': mySupportTask.errorMessage,
                                                                     'query': mySupportTask.confirmQuery,
                                                                     'acctID': mySupportTask.newAcctID,
                                                                     'sciPrg': mySupportTask.accountAttributes[
                                                                         'new_sciProgram'],
                                                                     'purpose': mySupportTask.accountAttributes[
                                                                         'new_purpose'],
                                                                     'userPrg': mySupportTask.accountAttributes[
                                                                         'new_userProgram'],
                                                                     'year': mySupportTask.accountAttributes[
                                                                         'new_year'],
                                                                     'spIds': spOrFd
                                                                     })








