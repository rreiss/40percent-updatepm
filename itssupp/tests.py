from django.test import TestCase
from  selenium import webdriver
import requests,json
#from .models import TestJsonData

# to run:  python manage.py test


# Create your tests here.

class FunctionalGUITestCase(TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox(executable_path=r'C:\Users\rreiss\Desktop\learn\geckodriver.exe')

    def test_there_is_homepage(self):
        self.browser.get('http://localhost:8000/')
        self.assertIn('itssupp',self.browser.page_source)

    def test_can_get_to_UpdateProjectManager(self):
        self.browser.get('http://localhost:8000/itssupp')
        text = self.browser.find_element_by_link_text("Update PM").click()
        self.assertIn('Update Project Manager', self.browser.page_source)

    def test_can_get_to_AbandonFDSP(self):
        self.browser.get('http://localhost:8000/itssupp')
        text = self.browser.find_element_by_link_text("Abandon FD/SP").click()
        self.assertIn('Abandon FDs', self.browser.page_source)
        text = self.browser.find_element_by_link_text("Production(jgi-prd)").click()

   # def tearDown(self):
       #self.browser.quit()

# class FunctionalJsonTestCase(TestCase):
#
#     def test_rest_client(self):    #in production,change this so as not to update customer data
#         status = 0
#         url = 'http://localhost:8000/itssupp/update-pm/'
#         requestData = { "submitted-by": 5981,  "pm-id": 13, "fdORsp-ids" : "1119177" }
#         mydata = json.dumps(requestData)
#         headers = {"Content-Type": "application/json", 'data': mydata}
#         # Call REST API
#         response = requests.post(url, data=mydata, headers=headers)  # post  request
#         status = response.status_code
#         self.assertEqual(status, 200)
#
#     def test_rest_client_response_is_valid(self):    #in production,change this so as not to update customer data
#         status = 0
#         url = 'http://localhost:8000/itssupp/update-pm/'
#         requestData = { "submitted-by": 5981,  "pm-id": 13, "fdORsp-ids" : "1119177" }
#         mydata = json.dumps(requestData)
#         headers = {"Content-Type": "application/json", 'data': mydata}
#         # Call REST API
#         response = requests.post(url, data=mydata, headers=headers)  # post  request
#         status = response.status_code
#         responseIsValid = True
#         #responseIsValid = get response data and check contents -  start here
#         self.assertTrue(responseIsValid)

# class FunctionalDataBaseConnection(TestCase):
# #  uses the temporary database for test  (sqlite, see settings.py and modelsx.py)
# #
#     def test_TestJsonData_object(self):  #using test database
#         #all data is saved to temp database (per django)
#         myJson = TestJsonData(submittedBy = 5981,pmID = 13,fdORsps = "1119177",server = 'int')
#         myJson.save(force_insert=True)
#         pulled_myJson = TestJsonData.objects.get(submittedBy=5981)
#         self.assertEqual(pulled_myJson.pmID,13)
#
#
#
#     def test_db_getJson(self):
#         newJsonObj = TestJsonData(submittedBy=13, pmID=13, fdORsps="1119177", server='int')
#         newJsonObj.save(force_insert=True)
#         myJson = TestJsonData.objects.all()[:1]
#         req = myJson[0].getJson()
#         fds = req["fdORsp-ids"]
#         self.assertEqual(fds, '1119177')

    # def test_db_badFD(self):
    #     #this test to make sure that appropriate errors will be display when the FD is not found
    #     newJsonObj = TestJsonData(submittedBy=13, pmID=13, fdORsps="1119177", server='int')
    #     newJsonObj.save(force_insert=True)
    #     myJson = TestJsonData.objects.all()[:1]
    #     req = myJson[0].getJson()
    #     fds = req["fdORsp-ids"]
    #     self.assertEqual(fds, '1119177')


