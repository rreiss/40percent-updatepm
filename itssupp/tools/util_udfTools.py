#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'
# created 7/25/17

import requests
from requests.auth import HTTPDigestAuth
import json
from xml.etree import ElementTree as ET



#name:  udfValidation  -

class udfTools():
    credentials = ('rjr', 'rjr')

    # ---------project udfs--tools--------------------------------------------------------------------------------
    # setURL
    # build the URL to fix the request for clarity API
    # inputs:  server  i.e. 'clarityint1.jgi-psf.org'
    #          sp  -  the sequencing project ID under test
    def setURLproject(self,sp,server):
        requestURL = 'https://' + server + '/api/v2/projects?name=' + str(sp)
        print("Getting project's URL from: " + requestURL)  #https://jgi-int.claritylims.com/api/v2/projects?name=1012803
        return requestURL


    # -------------------------------------------------------------------------------------------
    # connectToClarityAPIprojects
    #
    # inputs: requestURL
    # outputs: url of SP UDFs

    def connectToClarityAPIprojects(self,requestURL):
        projURL = ''
        try:
            # r= requests.get(requestURL, timeout=2)
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(requestURL, headers=headers, auth= self.credentials, stream=True, timeout=2)
        except:
            # status = r.status_code
            print("Opps! Can't get the request, Time out")
            return ''
        status = r.status_code
        if status == 200:
            # print("Valid Returned Status = ", status)
            # print(r.text) #this is the xml of the project base record
            root = ET.fromstring(r.text)
            # print (root.tag)
            for project in root.iter('project'):
                # print("from connecttoclarity")
                # print(project.attrib)
                projURL = project.get('uri')
                # print("-")
        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return projURL

    # -------------------------------------------------------------------------------------------
    # getProjectUDFs
    #
    # inputs: requestURL
    # outputs: url of SP UDFs

    def getProjectUDFs(self,requestURL):
        udfDict = {}
        try:
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(requestURL, headers=headers, auth=self.credentials, stream=True, timeout=2)
        except:
            print("Opps! Can't get the request, Time out")
            return {}
        status = r.status_code
        if status == 200:
            # print("Valid Returned Status = ", status)
            # print(r.text) # this is the xml of the project base record
            root = ET.fromstring(r.text)
            for node in root.iter():
                udfName = node.get('name')
                # print(udfName)
                udfValue = node.text
                # print(udfValue)
                udfDict[udfName] = udfValue
                # print('name= ' + str(root[i].get('name')) + ', value= '  +  str(root[i].text) )
                # print (root[i].text)


                # print (udfDict)

        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return udfDict


    # ---------sample udfs---tools-------------------------------------------------------------------------------

    def setURLsample(self,sampleId, server):
        requestURL = 'https://' + server + '/api/v2/samples?name=' + str(sampleId)
        print("Getting sample URL from: " + requestURL)  # https://jgi-int.claritylims.com/api/v2/samples?name=1012803
        #
        return requestURL

    # -------------------------------------------------------------------------------------------
    # connectToClarityAPIsamples
    #
    # inputs: requestURL
    # outputs: url of sampleUDFs
    def connectToClarityAPIsamples(self,requestURL):
        sampleURL = ''
        try:
            # r= requests.get(requestURL, timeout=2)
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(requestURL, headers=headers, auth=self.credentials, stream=True, timeout=2)

        except:
            # status = r.status_code
            print("Opps! Can't get the request, Time out")
            return ''
        status = r.status_code
        if status == 200:
            print("Valid Returned Status = ", status)
            #print(r.text) #this is the xml of the project base record
            root = ET.fromstring(r.text)
            #print (root.tag)
            for sample in root.iter('sample'):
                #print("from connecttoclarity")
                #print(sample.attrib)
                sampleURL = sample.get('uri')
                # print("-")
        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return sampleURL


    # -------------------------------------------------------------------------------------------
    # getSampleUDFs
    #
    # inputs: url of the clarity sample API
    # outputs: a dictionary of UDFS  (name of udf: value of udf)
    def getSampleUDFs(self,requestURL):
        udfDict = {}
        try:
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(requestURL, headers=headers, auth=self.credentials, stream=True,timeout=2)
        except:
            print("Opps! Can't get the request, Time out")
            return {}
        status = r.status_code
        if status == 200:
            #print("Valid Returned Status = ", status)
            #print(r.text) # this is the xml of the project base record
            root = ET.fromstring(r.text)
            for node in root.iter():
                udfName = node.get('name')
                #print(udfName)
                udfValue = node.text
                #print(udfValue)
                udfDict[udfName] = udfValue
                #print('name= ' + str(root[i].get('name')) + ', value= '  +  str(root[i].text) )
                #print (root[i].text)


                #print (udfDict)

        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return udfDict




    # ---------artifacts udfs---tools-------------------------------------------------------------------------------

    def setURLartifact(self,artifactName, server):
        requestURL = 'https://' + server + '/api/v2/artifacts?name=' + str(artifactName)
        print("Getting artifact's URL from: " + requestURL)  # https://jgi-int.claritylims.com/api/v2/artifacts?name=BXCCT
        #
        return requestURL

    # -------------------------------------------------------------------------------------------
    # connectToClarityAPIartifacts
    #
    # inputs: requestURL
    # outputs: url of artifacts
    def connectToClarityAPIartifacts(self,requestURL,type):
        artifactURL = ''
        try:
            # r= requests.get(requestURL, timeout=2)
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(requestURL, headers=headers, auth=self.credentials, stream=True, timeout=2)

        except:
            # status = r.status_code
            print("Opps! Can't get the request, Time out")
            return ''
        status = r.status_code
        if status == 200:
            print("Valid Returned Status = ", status)
            #print(r.text)  # this is the xml of the project base record
            root = ET.fromstring(r.text)
            #print(root.tag)

            for art in root.iter('artifact'):
                # print("from connecttoclarity")
                #print(art.attrib)
                artifactURL = art.get('uri')  #if library, get the last link to get artifacts
                if type=="pool":   #if we want the pool artifacts, get the 1st link to artifacts, the 2nd one is the flowcell
                    break
                # print(artifactURL)
        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return artifactURL

    # -------------------------------------------------------------------------------------------
    # getArtifactUDFs
    #
    # inputs: requestURL
    # outputs: url of artifact UDFs
    def getArtifactUDFs(self,requestURL):
        udfDict = {}
        try:
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(requestURL, headers=headers, auth=self.credentials, stream=True, timeout=2)
        except:
            print("Opps! Can't get the request, Time out")
            return {}
        status = r.status_code
        if status == 200:
            # print("Valid Returned Status = ", status)
            #print(r.text) # this is the xml of the project base record
            root = ET.fromstring(r.text)
            #self.getSchSampleOfLibraryUDFs(requestURL)  # put here just for debuggin
            #queuedList = self.getWorkflowsOfLibrary(requestURL,"QUEUED") # put here just for debuggin

            for node in root.iter('name'):
                libraryName = node.get('name')
                libraryNameValue = node.text
                udfDict["Library Name"]= libraryNameValue  #save the library name in the dictionary too

            for node in root.iter():
                udfName = node.get('name')
                #print(udfName)
                udfValue = node.text
                # print(udfValue)
                udfDict[udfName] = udfValue
                # print('name= ' + str(root[i].get('name')) + ', value= '  +  str(root[i].text) )
                # print (root[i].text)
                # print (udfDict)

        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return udfDict

    # -------------------------------------------------------------------------------------------
    # updateArtifactUDF
    #
    # inputs: requestURL, udfToUpdate, udf newValue
    # outputs: success = true

    def updateArtifactUDF(self, requestURL, udfToUpdate,udfNewValue):
        print ("in UpdateUDF")
        print (requestURL)
        try:
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(requestURL, headers=headers, auth=self.credentials, stream=True, timeout=2)
        except:
            print("Opps! Can't get the request, Time out")
            return {}
        status = r.status_code
        if status == 200:
            try:
                xmlOfArtifacts = r.text
                delimiter = '\"' + udfToUpdate + '\">'
                chunksList = xmlOfArtifacts.split(delimiter)  #separate into 2 chunks
                part1 = chunksList[0]
                part2 = chunksList[1]
                indx = part2.find("<")
                part2 = part2[indx:]
                updatedXML = part1 + delimiter + str(udfNewValue) + part2
                print (updatedXML)
                # put it back to url
                print("updated ", udfToUpdate, "in ", requestURL)
                r = requests.put(requestURL, data=updatedXML, headers=headers, auth=self.credentials, stream=True, timeout=2)
                status = r.status_code
                if status != 200:
                    print("*** Error, Unexpected Returned Status = ", status)
            except:
                print ("No UDF found or other error: ", udfToUpdate)
                return 700  #no udf found
        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return status


    # -------------------------------------------------------------------------------------------
    # getSchSampleOfLibraryUDFs  get the UDFS of the scheduled sample related to the library
    #
    # inputs: Clarity API URL  of  entity (library ) artifact record
    # outputs: dict of UDFs from the scheudule sample related to the artifact
    def getSchSampleOfLibraryUDFs(self,libraryApiURL):
        schSampleUDFs = {}
        try:
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(libraryApiURL, headers=headers, auth=self.credentials, stream=True, timeout=2)
        except:
            print("Opps! Can't get the request, Time out")
            return {}
        status = r.status_code
        if status == 200:
            # print("Valid Returned Status = ", status)
            #print(r.text)  # this is the xml of the project base record
            root = ET.fromstring(r.text)
            for sample in root.iter('sample'):
                sampleAttrib = sample.attrib
                schSampleURL = sampleAttrib.get("uri")
                print ("Getting Schedule Sample UDFs from ", schSampleURL)
                schSampleUDFs = self.getSampleUDFs(schSampleURL)

        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return schSampleUDFs


    # -------------------------------------------------------------------------------------------
    # getSchSampleArtifactUrl  get the URL  of the scheduled sample (related to the library) artifacts
    #
    # inputs: Clarity API URL  of  entity (library ) artifact record
    # outputs: dict of UDFs from the scheudule sample related to the artifact

    def getSchSampleArtifactUrl(self, libraryApiURL):
        schSampleArtifactUrl = ''
        try:
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(libraryApiURL, headers=headers, auth=self.credentials, stream=True, timeout=2)
        except:
            print("Opps! Can't get the request, Time out")
            return {}
        status = r.status_code
        if status == 200:
            #print(r.text)  # this is the xml of the project base record
            root = ET.fromstring(r.text)
            for sample in root.iter('sample'):
                sampleAttrib = sample.attrib
                schSampleURL = sampleAttrib.get("uri")
                print("Getting Schedule Sample artifacts URL from ", schSampleURL)
            try:
                headers = {'Content-Type': 'application/xml'}
                r = requests.get(schSampleURL, headers=headers, auth=self.credentials, stream=True, timeout=2)
            except:
                print("Opps! Can't get the request, Time out")
                return {}
            status = r.status_code
            if status == 200:
                # print("Valid Returned Status = ", status)
                # print(r.text) # this is the xml of the project base record
                root = ET.fromstring(r.text)
                for artifact in root.iter('artifact'):
                    artifactAttrib = artifact.attrib
                    schSampleArtifactUrl = artifactAttrib.get("uri")
                    #print (schSampleArtifactUrl)
            else:
                print("*** Error, Unexpected Returned Status = ", status)
        else:
            print("*** Error, Unexpected Returned Status = ", status)
        return schSampleArtifactUrl

    # -------------------------------------------------------------------------------------------
    # getWorkflowsFromArtifacts get  a list of the workflow stages of the  entity artifacts page that are in certain status
    #
    # inputs: artifactApiURL = Clarity API URL  of  entity (library ) artifact record,
    #           statusSought = status  of workflow that you are looking for (i.e. "QUEUED")
    # outputs: list of the queues that the entity is in as the inputted status
    def getWorkflowsFromArtifacts(self, artifactApiURL,statusSought):
        queueList = []
        try:
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(artifactApiURL, headers=headers, auth=self.credentials, stream=True, timeout=2)
        except:
            print("Opps! Can't get the request, Time out")
            return {}
        status = r.status_code
        if status == 200:
            # print("Valid Returned Status = ", status)
            # print(r.text)  # this is the xml of the project base record
            root = ET.fromstring(r.text)
            for workflow in root.iter('workflow-stage'):
                wfAttrib = workflow.attrib
                #print (wfAttrib)
                wfStatus = wfAttrib.get("status")
                wfqueue = wfAttrib.get("name")
                if wfStatus == statusSought:
                    queueList.append(wfqueue)
                    #print(wfStatus,wfqueue)
            #input("next")


        else:
            print("*** Error, Unexpected Returned Status = ", status)
        return queueList




    # ---------containers udfs---tools-------------------------------------------------------------------------------

    def setURLcontainer(self, containerName, server):
        requestURL = 'https://' + server + '/api/v2/containers?name=' + str(containerName)
        print(
            "Getting container's URL from: " + requestURL)  # https://jgi-int.claritylims.com/api/v2/containers?name=27-451459
        #
        return requestURL

    # -------------------------------------------------------------------------------------------
    # getClarityAPIcontainer
    #
    # inputs: requestURL
    # outputs: url of container
    def getClarityAPIcontainer(self, requestURL):
        containerURL = ''
        try:
            # r= requests.get(requestURL, timeout=2)
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(requestURL, headers=headers, auth=self.credentials, stream=True, timeout=2)

        except:
            # status = r.status_code
            print("Opps! Can't get the request, Time out")
            return ''
        status = r.status_code
        if status == 200:
            #print("Valid Returned Status = ", status)
            # print(r.text)  # this is the xml of the project base record
            root = ET.fromstring(r.text)
            # print(root.tag)

            for art in root.iter('container'):
                # print("from connecttoclarity")
                # print(art.attrib)
                containerURL = art.get('uri')  #
                #print(containerURL)
        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return containerURL

    # -------------------------------------------------------------------------------------------
    # getAttributeOfClarityAPIcontainer
    #
    # inputs: requestURL
    #         attribute = "containerType" or "numberOfWells"
    # outputs: attribute value
    def getAttributeOfClarityAPIcontainer(self, requestURL,attribute):
        attributeValue = ''
        try:
            # r= requests.get(requestURL, timeout=2)
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(requestURL, headers=headers, auth=self.credentials, stream=True, timeout=2)
        except:
            print("Opps! Can't get the request, Time out")
            return ''
        status = r.status_code
        if status == 200:
            root = ET.fromstring(r.text)
            if attribute=="containerType":
                for type in root.iter('type'):
                    attributeValue = type.get('name')
            elif attribute == "numberOfWells":
                for node in root.iter('occupied-wells'):
                    udfName = node.get('occupied-wells')
                    attributeValue = node.text

            else:
                print ("error, wrong value in parameter")
        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return attributeValue

    # -------------------------------------------------------------------------------------------
    # getLibURLSfromClarityAPIcontainer
    #
    # inputs: requestURL
    #
    # outputs: list of urls of libraries in container
    def getLibURLSfromClarityAPIcontainer(self, requestURL):
        libURLlist = []
        try:
            # r= requests.get(requestURL, timeout=2)
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(requestURL, headers=headers, auth=self.credentials, stream=True, timeout=2)
        except:
            # status = r.status_code
            print("Opps! Can't get the request, Time out")
            return ''
        status = r.status_code
        if status == 200:
            root = ET.fromstring(r.text)
            for placement in root.iter('placement'):
                libURL = placement.get('uri')
                libURLlist.append(libURL)
        else:
            print("*** Error, Unexpected Returned Status = ", status)
        return libURLlist



            # history -
        # 8-2  created