#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'
# created 6/25/19

from jira import JIRA

# tools to interface with jira by adding comments and transitioning tickets

class jiraTools():
    myCredentials = ('itssuppws', 'itssuppws_2019')
    jira = JIRA(basic_auth=myCredentials,
                options={'server': 'https://issues.jgi.doe.gov'})


    # -------------------------------------------------------------------------------------------
    # connectTojira
    #
    # inputs: none
    # outputs: 0 = no error,  999 - can't connect to jira

    def connectToJira(self):
        try:
            self.jira = JIRA(basic_auth=self.myCredentials,
                        options={'server': 'https://issues.jgi.doe.gov'})
        except:
            print("Opps! Can't connect to JIRA!")
            return 999
        return 0

    # -------------------------------------------------------------------------------------------
    # addComment
    # add comment to an itssupp ticket
    # inputs:
    #     ticketNum  = the ITSSUPP ticket number (just numbers)
    #     message = text for the comment
    # outputs:

    def addCommentToJira(self,ticketNum,message,roleValue="Users"):
        jiraticketinfo = ""
        if (ticketNum):
            jiraticketinfo = "ITSSUPP-" + str(ticketNum)
            # add comment to jira issue (if exists)
            issue = None
            try:
                issue = self.jira.issue(jiraticketinfo)
            except:
                print('Jira Issue does not exist: ' + jiraticketinfo)
                pass

            if issue:
                # add a comment to jira ticket
                #roleValue = 'Administrators'
                visibility = {'type': 'role', 'value': roleValue}
                self.jira.add_comment(jiraticketinfo, message,visibility)

        return jiraticketinfo

    # -------------------------------------------------------------------------------------------
    # transitionTicket
    # transition ticket to the inputted state (string).
    # do not use if requires transition screen input
    #     ticketNum  = the ITSSUPP ticket number (just numbers)
    #     transitionState = the string value of the workflow transition that you want to move
    #     ticket to
    # outputs:  none
    def transitionTicket(self,ticketNum,transitionState):
        jiraticketinfo = ""
        if (ticketNum):
            jiraticketinfo = "ITSSUPP-" + str(ticketNum)
            # add comment to jira issue (if exists)
            issue = None
            try:
                issue = self.jira.issue(jiraticketinfo)
            except:
                print('Jira Issue does not exist: ' + jiraticketinfo)
                pass

            if issue:
                # transition ticket to transition State
                issue = self.jira.issue(jiraticketinfo)
                transitions = self.jira.transitions(issue)
                for t in transitions:
                    if t['name'] == transitionState:
                        desiredTransition = t['id']
                        self.jira.transition_issue(issue,desiredTransition)



