#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'
# created 7/25/17
import requests
import json
import cx_Oracle
#from wsTools import wsTools
import os


class dbTools():

    def __init__(self):
            self.errorCnt = 0
            self.errorList =[]
            self.conn = None
            self.close()   #default connect to development db

    # ----------------------------------------------------------------------------------------
    #  this connects to the data base requested
    # inputs:
    #       database - 'prod' to connect to genprd2,  everything else will connect to genint1
    def connect(self,database):
        if self.conn is None:  # no database connected
             self.errorCnt = 0
             # print("running this from: " + os.getcwd())
             oracle_vars = dict((a, b) for a, b in os.environ.items())
             # print("ORACLE_HOME = " + str(oracle_vars['ORACLE_HOME']))
             if database == 'Prod':
                db = cx_Oracle.connect('ussrw', 'funnyb1z',
                                    'clarity-prddb01.jgi.doe.gov:1521/genprd2')  # use 'ussro','ussro' for  read only
                print ("connected to genprd2 (prod)")
             elif database == 'Int': #connect to jgi-int
                db = cx_Oracle.connect('ussrw', 'funnyb1z', 'clarity-devdb01.jgi.doe.gov:1521/genint2')  # use 'ussro','ussro' for  read only
                print("connected to genint2 (int)")
             elif database == 'Dev':  # connect to jgi-dev
                db = cx_Oracle.connect('ussrw', 'funnyb1z',
                                       'clarity-devdb01.jgi.doe.gov:1521/gendev2')  # use 'ussro','ussro' for  read only
                print("connected to dwint1 (dev)")
             else:  #something wrong, db not specifiec
                print("****ERROR - DB not defined. No connection!!! ***")
                exit()

             # print('the DB version is ' + db.version)
             self.conn = db
             self.cursor = db.cursor()

    # ----------------------------------------------------------------------------------------
    #  this closes  the data base requested, if opened
    #
    #
    def close(self):
        if self.conn is not None:
            self.conn.close()
            self.conn = None
    # ----------------------------------------------------------------------------------------
    # builds the query based on the attributes
    #  this querys the data base and returns the value of the field requested. Only single value. search is '='
    # inputs:
    #       field - name of the database table field that you want to get data from, i.e. SEQUENCING_PROJECT_NAME
    #       table - name of database table
    #       key - the field name for the key to search, i.e. SAMPLE_ID
    #       value - the value of the key to search the database   (method uses =)
    def getDBattibute(self,selectField,fromDbTable,whereKey,inValue):
                query = 'select ' + selectField + ' from ' + fromDbTable + ' where ' + whereKey  + ' in ' + inValue
                self.cursor.execute(query)
                for row in self.cursor:
                   attr = str(row[0])

                return attr

    # ----------------------------------------------------------------------------------------
    #  simply runs the query and returns the single value found
    # inputs:
    #       query - string that has full query command
    # outputs:
    #       attr - the result of the query (a single attribute request)

    # looks redundant to getDBattibute  (except needs the entire query to run)

    def doQuery(self, query):      # returns 1st occourance
        print(query)
        self.cursor.execute(query)
        attr = ''
        for row in self.cursor:
            attr = str(row[0])
        return attr


    # ----------------------------------------------------------------------------------------
    #  runs the query and returns the number of rows
    # inputs:
    #       query - string that has full query
    # outputs:
    #       count  - the result of the query (a single attribute request)

    def doQueryGetAllRows(self, query):  # returns list
        tableList= []
        count = 0
        self.cursor.execute(query)
        i = 0
        #print (query)
        for row in self.cursor:
            attr = str(row[0])
            tableList.append(attr)
            i += 1


        return tableList


    # currently not used
    def doQueryGetAllRowsCols(self, query):  # returns dict,  key = string, value = list
        tableDict = {}

        self.cursor.execute(query)

        for row in self.cursor:
            #print (row)
            key = str(row[0])
            value = []
            value.append(str(row[1]))
            value.append(str(row[2]))
            tableDict[key]=value

        return tableDict


    # ----------------------------------------------------------------------------------------
    #  simply runs a update statement
    # inputs:
    #       query - string that has full  update statement
    def doUpate(self, statement):

        try:
            self.cursor.execute(statement)
            self.cursor.execute("commit")

        except cx_Oracle.DatabaseError as e:
            print ("*** error detected while in doUpdate:")
            print(e)
            self.errorList.append(e)

        return self.errorList


    # ----------------------------------------------------------------------------------------
    #  this querys the data base and returns the value(s) of the field requested as a list. Multiple values may be returned. search is '='
    # inputs:
    #       field - name of the database table field that you want to get data from, i.e. SEQUENCING_PROJECT_NAME
    #       table - name of database table
    #       key - the field name for the key to search, i.e. SAMPLE_ID
    #       value - the value of the key to search the database   (method uses =)
    # outputs:  returns the 1st object (should only be one) of each row of query result (i.e. find all sow_items with sp=x)
    # currently not used  (redundant method)
    def getDBattibuteList(self, selectField, fromDbTable, whereKey, inValue):
        query = 'select ' + selectField + ' from ' + fromDbTable + ' where ' + whereKey + ' in ' + inValue
        # print (query)
        self.cursor.execute(query)
        attrList =[]
        for row in self.cursor:
            attr = str(row[0])
            attrList.append(str(attr))
        return attrList

    # ----------------------------------------------------------------------------------------
    #  this gets the status from the cv table that matches the current_status_id of the table in question
    # inputs:
    #       type - the type of status: "sample", "sp", "sow", "fd","ap", "at"
    #       id - the id of the sample, sp, sow, fd, ap or at  (ie. the sample_id value)
    # outputs:  returns the status (from cv table of the id requested)
    #
    #  This is an example of what the sql statement would look like when formed by this method:
    #       select
    #       cv.status
    #       from uss.dt_sequencing_project t
    #       LEFT  JOIN uss.DT_SEQ_PROJECT_STATUS_CV cv ON cv.STATUS_ID = t.CURRENT_STATUS_ID
    #       where t.SEQUENCING_PROJECT_ID = 1047857;
    # not used in this project yet
    def getStatus(self,type, id):
                #print("DB query for getDBattribute():")
                #query = 'select ' + field + ' from ' + table + ' where ' + key  + ' in ' + value
                query = ''
                attr = ''
                if type =='sp':
                    tableName = 'uss.dt_sequencing_project'
                    cvTable = 'uss.DT_SEQ_PROJECT_STATUS_CV'
                    fieldKey = 'SEQUENCING_PROJECT_ID'
                elif type =='sample':
                    tableName = 'uss.dt_sample'
                    cvTable = 'uss.dt_sample_status_cv'
                    fieldKey = 'SAMPLE_ID'
                elif type == 'sow':
                    tableName = 'uss.dt_sow_item'
                    cvTable = 'uss.dt_sow_item_status_cv'
                    fieldKey = 'sow_item_id'
                elif type == 'fd':
                    tableName = 'uss.dt_final_deliv_project'
                    cvTable = 'uss.DT_FINAL_DELIV_PROJ_STATUS_CV'
                    fieldKey = 'final_deliv_project_id'
                elif type == 'ap':
                    tableName = 'uss.DT_ANALYSIS_PROJECT'
                    cvTable = 'uss.DT_ANALYSIS_PROJECT_STATUS_CV'
                    fieldKey = 'ANALYSIS_PROJECT_ID'
                elif type == 'at':
                    tableName = 'uss.DT_ANALYSIS_TASK'
                    cvTable = 'uss.DT_ANALYSIS_TASK_STATUS_CV'
                    fieldKey = 'ANALYSIS_TASK_ID'

                query = 'select cv.status from ' + tableName + ' t LEFT JOIN ' + cvTable + \
                        ' cv ON cv.STATUS_ID = t.CURRENT_STATUS_ID where t.' + fieldKey + ' = ' + str(id)
                #print (query)
                self.cursor.execute(query)
                for row in self.cursor:
                    attr = str(row[0])
                return attr
    # ----------------------------------------------------------------------------------------
    #  this gets the embargo days from the cv table
    # inputs:
    #
    #       id - the embargo_days_id
    # outputs:  returns the embargo days (from cv table of the id requested)
    #
    #  This is an example of what the sql statement would look like when formed by this method:
    #       select
    #       cv.status
    #       from uss.dt_sequencing_project t
    #       LEFT  JOIN uss.DT_SEQ_PROJECT_STATUS_CV cv ON cv.STATUS_ID = t.CURRENT_STATUS_ID
    #       where t.SEQUENCING_PROJECT_ID = 1047857;
    #not used in this project yet
    def getEmbargoDays(self,id):
                #query = 'select ' + field + ' from ' + table + ' where ' + key  + ' in ' + value
                query = 'select EMBARGO_DAYS from uss.DT_EMBARGO_DAYS_CV  where EMBARGO_DAYS_ID = ' + str(id)
                print (query)
                self.cursor.execute(query)
                for row in self.cursor:
                    attr = str(row[0])
                return attr
# -------------------------------------------------------------------------------------------








