import requests
import json, sys

class wsTools():
    # ----------------------------------------------------------------------------------------------
    # returns json string if request status= 200, returns {} if request is bad or times out
    # same as checkforValidRequest
    def runWS(self, requestURL):
        jsonstring = {}
        try:
            r = requests.get(requestURL, timeout=36000)  #timeout is 10 minutes
        except:
            # status = r.status_code
            print("*** Error ! Opps! Can't get the request, Time out")
            print("Unexpected error:", sys.exc_info()[0])
            return {}
        status = r.status_code
        if status == 200:
            print("Valid Returned Status = ", status)
            jsonstring = r.json()
            print(json.dumps(jsonstring, sort_keys=True, indent=4))  # this prints all the fields as a json object
        else:
            print("*** Error, Unexpected Returned Status = ", status)
            print(r.text)
        return jsonstring



#these are not used in 40% project yet, they were created for the ws tools tests

    # ----------------------------------------------------------------------------------------------
    # returns json string if request status= 200, returns {} if request is bad or times out
    def checkforValidRequest(self,requestURL):
        jsonstring = {}
        try:
            r = requests.get(requestURL, timeout=36000)
        except Exception as e:
            # status = r.status_code
            print("Opps! Can't get the request, Time out")
            print("Unexpected error:", sys.exc_info()[0])
            print ("Exception: ", str(e))
            return {}
        status = r.status_code
        if status == 200:
            print("Valid Returned Status = ", status)
            jsonstring = r.json()
            print(json.dumps(jsonstring, sort_keys=True, indent=4))  # this prints all the fields as a json object
        else:
            print("*** Error, Unexpected Returned Status = ", status)
            print (r.text)
        return jsonstring

    # ----------------------------------------------------------------------------------------------
    def checkForMissingSPID(self,requestURL):
        #  missing spid , expect
        errorCnt = 0
        r = requests.get(requestURL)
        status = r.status_code
        if status == 404:
            print("Expected Returned status for invalid request(missing SP ID) = ", status)
        else:
            print("***Error, UnExpected Returned status for invalid request(missing SP ID) = ", status, ", Expected 404")
            print("URL used: ", requestURL)
            errorCnt = 1

        return errorCnt

    # ----------------------------------------------------------------------------------------------
    def checkForInvalidCommands(self,requestURL):

        #  invalid 'POST' , expect 405
        errorCnt=0
        r = requests.post(requestURL)
        status = r.status_code
        if status == 405:
            print("Expected Returned status for invalid POST = ", status)
        else:
            print("***Error, UnExpected Returned status for invalid POST = ", status, ", Expected 405")
            print("URL used: ", requestURL)
            errorCnt = errorCnt+1

        # invalid 'PUT' , expect 405
        r = requests.put(requestURL)
        status = r.status_code
        if status == 405:
            print("Expected Returned status for invalid PUT = ", status)
        else:
            print("***Error, UnExpected Returned status for invalid PUT = ", status, ", Expected 405")
            print("URL used: ", requestURL)
            errorCnt = errorCnt + 1

        # invalid 'DELETE' , expect 405
        r = requests.delete(requestURL)
        status = r.status_code
        if status == 405:
            print("Expected Returned status for invalid DELETE = ", status)
        else:
            print("***Error, UnExpected Returned status for invalid DELETE = ", status, ", Expected 405")
            print("URL used: ", requestURL)
            errorCnt = errorCnt + 1

        return errorCnt
    # ----------------------------------------------------------------------------------------------
    def checkForInvalidRequest(self,requestURL):
        #  bad spid , expect 404
        errorCnt = 0
        print("Invalid Request = "  + requestURL)
        r = requests.get(requestURL, timeout=2)
        status = r.status_code
        if status == 404:
            print("Expected Returned status for invalid request (bad SP ID) = ", status)
        else:
            print("***Error, UnExpected Returned status for invalid request(bad SP ID) = ", status, ", Expected 404")
            errorCnt = 1

        return errorCnt



    # ----------------------------------------------------------------------------------------------
    def checkForInvalidPOST(self,requestURL):
        #  invalid 'POST' , expect 405
        errorCnt = 0
        r = requests.post(requestURL)
        status = r.status_code
        if status == 405:
            print("Expected Returned status for invalid POST = ", status)
        else:
            print("***Error, UnExpected Returned status for invalid POST = ", status, ", Expected 405")
            print("URL used: ", requestURL)
            errorCnt = errorCnt + 1

        return errorCnt

    # ----------------------------------------------------------------------------------------------
    def checkForInvalidDELETE(self,requestURL):
        errorCnt = 0
        # invalid 'DELETE' , expect 405
        r = requests.delete(requestURL)
        status = r.status_code
        if status == 405:
            print("Expected Returned status for invalid DELETE = ", status)
        else:
            print("***Error, UnExpected Returned status for invalid DELETE = ", status, ", Expected 405")
            print("URL used: ", requestURL)
            errorCnt = errorCnt + 1

        return errorCnt

    # ----------------------------------------------------------------------------------------------
    def checkForInvalidPUT(self,requestURL):
        errorCnt = 0
        # invalid 'PUT' , expect 405
        r = requests.put(requestURL)
        status = r.status_code
        if status == 405:
            print("Expected Returned status for invalid PUT = ", status)
        else:
            print("***Error, UnExpected Returned status for invalid PUT = ", status, ", Expected 405")
            print("URL used: ", requestURL)
            errorCnt = errorCnt + 1

        return errorCnt