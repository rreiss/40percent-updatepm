from django.urls import path
from django.conf.urls import url
from django.contrib import admin
#from rest_framework.urlpatterns import format_suffix_patterns

from django.views.generic import RedirectView
from . import views
app_name = 'itssupp'


urlpatterns = [
    path('', views.projectManagerGui.index, name='index'),
    path('gui_updatePMpage/', views.projectManagerGui.gui_UpdatePMpage, name='gui_updatePMpage'),
    path('infoUpdatePM/', views.projectManagerGui.infoUpdatePM),
    path('gui_abandonPage', views.abandonFDSPGui.gui_abandonPage, name='gui_abandonPage'),
    path('gui_deleteFDPage', views.deleteFDGui.gui_deleteFDPage, name='gui_deleteFDPage'),
    path('gui_deletePage', views.deleteSPGui.gui_deletePage, name='gui_deletePage'),
    path('gui_changeAccountPage/', views.changeAccountGui.gui_changeAccountPage, name='gui_changeAccountPage'),
    path('infoAbandon/', views.abandonFDSPGui.infoAbandon, ),
    path('infoDeleteFD/', views.deleteFDGui.infoDeleteFD, ),
    path('infoDelete/', views.deleteSPGui.infoDelete, ),
    path('infoChangeAccount/', views.changeAccountGui.infoChangeAccount),
    url(r'^update-pm/', views.update_PM.as_view()),
    url(r'^favicon\.ico$',RedirectView.as_view(url='/static/images/favicon.ico')),
]