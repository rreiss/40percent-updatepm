#from wsTools import wsTools
#from util_udfTools import udfTools

from itssupp.tools.util_dbTools import dbTools
from itssupp.tools.util_jiraTools import jiraTools


#gets the samples, sows, sps, aps, ats  and FDs that are related to FD and deletes them. Also delete FD
#user inputs on command line:
#         server  = prod or dev or int
#         itssupp jira ticket number
#         the fds or sps that need to be delete and children. if sps are given, the associated fds will also be deleteed
# will output, all decendents of FD.  The update commands to delete these entities, the query to confirm status update,
# and the URL and sample list to use in routetoworkflow to remove samples from clarity  queues


class Support_deleteSPandChildren():
    def __init__(self):
        self.myDB = dbTools()
        self.myJira = jiraTools()
        self.errorCnt = 0
        self.errorMessage = ""
        self.sComment = ""
        self.server = ''
        self.route2workflowURL = "https://pps-int.jgi.doe.gov/route-to-workflow"
        self.confirmQuery = ""
        self.sampleSet = set()
        self.sowSet = set()
        self.spSet = set()
        self.apSet = set()
        self.atSet = set()
        self.fdSet = set()
        self.servername= ""

    #-----------------------------------------------------
    # get all childrend of related FDs  and delete
    # inputs:  server -  "prod"  from production (pluss-genprd1), 'dev' for development (plus-dwint1)
    def doDelete(self,server,ticketNum,function,spFDs):
        print("in doDelete")
        print ("server= ",server,", ITSSUPP-",ticketNum,", Function= ",function,"SPs or FDs = ",spFDs)

        if server != 'Prod' and server != 'Dev' and server != 'Int':
            self.errorCnt = 1
            self.errorMessage = "No such server!"
        else:
            self.server = server
            #set the url for routetoworkflow
            if self.server == 'Prod':
                self.route2workflowURL = "https://pps-prd.jgi.doe.gov/route-to-workflow"
                self.servername = "jgi-prd"
            elif self.server == 'Dev':
                self.route2workflowURL = "https://pps-dev.jgi.doe.gov/route-to-workflow/"
                self.servername = "jgi-dev"
            elif self.server == 'Int':
                self.route2workflowURL = "https://pps-int.jgi.doe.gov/route-to-workflow"
                self.servername = "jgi-int"
            else:
                self.route2workflowURL = ""
                self.servername = ""
                self.errorCnt = 1
                self.errorMessage = "No such server!"

            if (self.errorCnt==0):  #no errors.  continue.
                self.myDB.connect(self.server)
                self.myJira.connectToJira()
                # set status comment for db update
                # self.sComment = 'ITSSUPP-' + ticketNum + ' auto-updated by Support_updatePM.py'
                if (ticketNum):
                    jiraticketinfo = "ITSSUPP-" + str(ticketNum)
                else:
                    jiraticketinfo = ""
                self.sComment = jiraticketinfo + '  auto-updated by ITS Support Web Service(deleteFDorSPandChildren.py)'

                self.spsOrFds = spFDs   # save the sp ids (or fd ids ) from the input
                print (function)

                if function =="SPchildren":
                    self.functionText = "deleteSPandChildren"
                    self.deleteSPandChildren()
                    if (ticketNum):  # if jira ticket number was inputted
                        if self.errorCnt >0 :
                            message = 'An error occured during deletion process of SPs =' + spFDs + ' and children:\n ' + str(self.errorMessage)
                        else:
                            message = 'SPs =' + spFDs + ' and children have been deleted by ITS Support Web Service'
                        self.addCommentsAndClose(ticketNum,message)
                # when just running test functions, do not update database, no db status comment is needed

                elif function == "SPchildrenTest":
                    self.functionText = "deleteSPandChildrenTest"
                    self.deleteSPandChildren()
                else:
                    self.errorCnt = 1
                    self.errorMessage = "No such function!"

        print("error count=", self.errorCnt)

    # ----------------------------------
    # add comments to jira and close jira ticket
    #
    # ---------------------------------

    def addCommentsAndClose(self, ticketNum,message):
        self.myJira.addCommentToJira(ticketNum, message)
        message = "Use this query to confirm the statuses:\n" + self.confirmQuery
        roleValue = "Developers"
        self.myJira.addCommentToJira(ticketNum, message, roleValue)
        #do this for now, until get the api for route to workflow,  must do by hand for now
        if self.errorCnt==0:
            message = "To do:  check if any samples need to be removed from queues.\nUse this set of samples:\n"
            for s in self.sampleSet:
                message += s + "\n"
            message += "with routeToWorkflow controller:\n" + str(self.route2workflowURL)
            self.myJira.addCommentToJira(ticketNum, message, roleValue)
        # will close automatically once the route to workflow is working automatically
       # self.myJira.transitionTicket(ticketNum, 'Resolve and Close')




    # -----------------------------------------------------
    # delete SP and children
    # SPs, samples and sows only
    # ------------------------------------------------------
    def deleteSPandChildren(self):
        spId = self.getSPsToProcess()
        if (self.errorCnt == 0):  # no errors.  continue.
            if (spId):

                # get all samples from DB
                query = 'select sam.SAMPLE_ID from uss.dt_sequencing_project sp ' \
                        'left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id ' \
                        'left join uss.dt_m2m_samplesowitem m2sam on sow.sow_item_id = m2sam.sow_item_id ' \
                        'left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id ' \
                        'where sp.sequencing_project_id in (' + str(spId) + ') and sam.current_status_id not in (16)'
                print(query)
                sampleList = self.myDB.doQueryGetAllRows(query)
                sampleSet = set(sampleList)
                self.sampleSet = sampleSet

                # get all sows
                query = 'select sow.SOW_ITEM_ID from uss.dt_sow_item sow ' \
                        'where sow.sequencing_project_id in (' + str(spId) + ') and sow.current_status_id not in (10)'
                print (query)
                sowList = self.myDB.doQueryGetAllRows(query)
                sowSet = set(sowList)
                self.sowSet = sowSet

                # get all sps
                query = 'select sp.sequencing_project_id from uss.dt_sequencing_project sp ' \
                        'where sp.sequencing_project_id in (' + str(spId) + ') and sp.current_status_id not in (9)'
                print(query)
                spList = self.myDB.doQueryGetAllRows(query)
                spSet = set(spList)
                self.spSet = spSet

                if self.functionText == "deleteSPandChildren":   # update database for  non-test function
                    self.deleteSamples(sampleSet)
                    self.deleteSows(sowSet)
                    self.deleteSPs(spSet)

                query = ("select distinct "
                         "sp.sequencing_project_id as spid, "
                         "spcv.STATUS as spstatus, "
                         "sp.SEQUENCING_PROJECT_NAME, "
                         "sam.SAMPLE_ID as samid, "
                         "samcv.STATUS as samstatus,"
                         "sow.SOW_ITEM_ID as sowid,"
                         "sowcv.STATUS as sowstatus "
                         "from uss.dt_sequencing_project sp "
                         "left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id "
                         "left join uss.dt_m2m_samplesowitem m2sam on sow.sow_item_id = m2sam.sow_item_id "
                         "left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id "
                         "LEFT JOIN uss.dt_sow_item_status_cv sowcv ON sowcv.STATUS_ID = sow.CURRENT_STATUS_ID "
                         "LEFT JOIN uss.dt_sample_status_cv samcv ON samcv.STATUS_ID = sam.CURRENT_STATUS_ID "
                         "LEFT JOIN uss.DT_SEQ_PROJECT_STATUS_CV spcv ON spcv.STATUS_ID = sp.CURRENT_STATUS_ID "
                         "where  sp.sequencing_project_id in (" + spId + ");")
                self.confirmQuery = query

    #-----------------------------------------------------
    # get the input from user, can be either SPs  or FDs,
    # separated by comma.  Will return the list of FDs associated with command
    #
    def getFDsToProcess(self):
        fdORsp = self.spsOrFds
        fdId = []
        fdCountquery = "select count(fd.final_deliv_project_id) from uss.dt_final_deliv_project fd where fd.final_deliv_project_id in (" + fdORsp + ")"
        fdCount = self.myDB.doQuery(fdCountquery)
        if (fdCount == '0'):  # the user input are not FDs, try sps
            spCountquery = "select count(sp.sequencing_project_id) from uss.dt_sequencing_project sp where sp.sequencing_project_id in (" + fdORsp + ")"
            spCount = self.myDB.doQuery(spCountquery)
            if int(spCount) > 0:  # sps were inputted
                query = "select (sp.final_deliv_project_id) from uss.dt_sequencing_project sp where sp.sequencing_project_id in (" + fdORsp + ")"
                fdIdList = self.myDB.doQueryGetAllRows(query)  # get the FDs associated with SPs
                fdId = ",".join(map(str, set(fdIdList)))
            else:
                print("***** no FDs or SPs found in DB ******")
                self.errorCnt += 1
                self.errorMessage = "***** no FDs or SPs found in DB ******"

        else:
            fdId = fdORsp
        return fdId


    # -----------------------------------------------------
    # get the  SPs input from user,
    # separated by comma.  Will return the list of SPs associated with command
    #

    def getSPsToProcess(self):
        spList = self.spsOrFds
        print (self.spsOrFds)
        spID = spList
        spCountquery = "select count(sp.sequencing_project_id) from uss.dt_sequencing_project sp where sp.sequencing_project_id in (" + spList + ")"
        print (spCountquery)
        spCount = self.myDB.doQuery(spCountquery)
        if int(spCount) <= 0:  # sps were inputted
            print("***** no SPs found in DB ******")
            self.errorCnt += 1
            self.errorMessage = "***** no SPs found in DB ******"
            spID = ""
        return spID



    #---------------------------------------------------------------------------------
    def deleteSamples(self, sampleSet):

        # delete the samples
        print("-------------------------------------------")
        print("delete samples:", sampleSet, "if not deleted already")
        if len(sampleSet)==0 :
            print("*** no samples to delete")
            #self.sampleSet.add("No Samples to Delete")
            self.sampleSet = "***None***"

        else:
            y = ",".join(map(str, sampleSet))
            query = "update uss.dt_sample set current_status_id=16, " \
                    "status_comments = '" + self.sComment + "',status_date = sysdate " \
                                                            "where sample_id in (" + y + ") " \
                                                                               "and current_status_id not in (16)"
            print(query)
            errorList = self.myDB.doUpate(query)
            if len(errorList)>0:
                print ("samples not deleted")
                self.errorCnt += 1
                self.errorMessage = errorList[len(errorList) - 1]
                self.sampleSet.clear()




    # ---------------------------------------------------------------------------------
    def deleteSows(self,sowSet):

        # delete the sows
        print("-------------------------------------------")
        print("delete sows:", sowSet, "if not deleted already")
        if len(sowSet)==0 :
            print("*** no sows to delete")
            self.sowSet = "***None***"

        else:
            y = ",".join(map(str, sowSet))
            query = "update uss.dt_sow_item set current_status_id=10, " \
                    "status_comments = '" + self.sComment + "',status_date = sysdate " \
                                                            "where sow_item_id in (" + y + ") " \
                                                                      "and current_status_id not in (10)"
            print(query)
            errorList = self.myDB.doUpate(query)
            if len(errorList) > 0:
                print("sows not deleted")
                self.errorCnt += 1
                self.errorMessage = errorList[len(errorList) - 1]
                self.sowSet.clear()

    # ---------------------------------------------------------------------------------
    def deleteSPs(self,spSet):

        # delete the sps
        print("-------------------------------------------")
        print("delete SPs:", spSet, "if not deleted already")
        if len(spSet)==0 :
            print("*** no SPs to delete")
            self.spSet = "***None***"

        else:
            y = ",".join(map(str, spSet))
            query = "update uss.dt_sequencing_project  set current_status_id=9, " \
                    "status_comments = '" + self.sComment + "',status_date = sysdate " \
                                                            "where sequencing_project_id in (" + y + ") " \
                                                                      "and current_status_id not in (9)"
            print(query)
            errorList = self.myDB.doUpate(query)
            if len(errorList) > 0:
                print("SPs not deleted")
                self.errorCnt += len(errorList)
                self.errorMessage = errorList[len(errorList)-1]
                self.spSet.clear()
                self.spSet = "***None***"



            # errDict = self.myDB.doUpate(query)
            # print (list(errDict))
            # if len(list(errDict)) > 1:
            #     self.errorCnt += list(errDict)[1]
            #     self.errorMessage = errDict[1]









