#from wsTools import wsTools
#from util_udfTools import udfTools

from itssupp.tools.util_dbTools import dbTools
from itssupp.tools.wsTools import wsTools
from itssupp.tools.util_udfTools import udfTools
from itssupp.tools.util_jiraTools import jiraTools



import json
import datetime
import time


'''
see https://docs.google.com/document/d/15IdvVYW7CunFPZqQ2r_QjCbWhXee0Htmr-uZkI2Rb5k/edit

To update the project manager for each given sequencing project, the following steps must happen:
1.	given a list of FD or Sps
2.  get the list of FDs, related SPs and related Aps
3.	Update the following USS.dt fields with the new PM id (“new-pm-id”):
•	uss.dt_sequencing_project/SEQUENCING_PROJECT_MANAGER_ID
•	uss.DT_ANALYSIS_Project/PROJECT_MANAGER_ID
•	uss.dt_final_deliv_project/DEFAULT_PROJECT_MANAGER_ID

4.	Update Clarity Project Process UDFs
•	Sequencing Project Manager 
•	Sequencing Project Manager ID 
'''



class Support_updatePM():
    def __init__(self):
        self.myDB = dbTools()
        self.myJira = jiraTools()
        self.errorCnt = 0
        self.errorMessage = ""
        self.sComment = ""
        self.server = ''
        self.functionText = ""
        self.confirmQuery = ""
        self.sampleSet = ""
        self.sowSet = ""
        self.spSet = ""
        self.apSet = ""
        self.atSet = ""
        self.fdSet = ""
        self.servername= ""
        self.submittedBy = ""   #to be supplied by "submitted-by" on WS request, otherwise default for now
        self.wsTool = wsTools()
        self.myUDF = udfTools()
        self.projectManagerName = ""
        self.urlSet = []
        self.status = ""

    #-----------------------------------------------------
    # get all childrend of related FDs
    # inputs:  server -  "Prod"  from production (genprd2),
    #                     'Dev' for development (plus-dwint1),
    #                     'Int' for intergration (plus-genint1)
    #           ticktenum - the ITSSUPP ticket num, can be blank
    #           function -  'UpdatePM'
    #                       'UpdatePMtest'  for testing, will not update database or UDFS
    def doUpdatePM(self,server,ticketNum,function,spFDs,pmID,submittedBy):
        self.myJira.connectToJira()
        self.submittedBy = str(submittedBy)
        print("in doUpdatePM")
        print (server,ticketNum,function,spFDs)
        if server != 'Prod' and server != 'Dev' and server != 'Int':
            self.errorCnt = 1
            self.errorMessage = "No such server!"
        else:
            self.server = server
            #set the url for routetoworkflow
            if self.server == 'Prod':
                self.servername = "jgi-prd"
                self.clarityURL = "https://jgi-prd.claritylims.com/"
            elif self.server == 'Dev':
                self.servername = "jgi-dev"
                self.clarityURL = "https://jgi-dev.claritylims.com"
            elif self.server == 'Int':
                self.servername = "jgi-int"
                self.clarityURL = "https://jgi-int.claritylims.com"
            else:
                #self.route2workflowURL = ""
                self.servername = ""
                self.clarityURL = ""
                self.errorCnt = 1
                self.errorMessage = "No such server!"

            if (self.errorCnt==0):  #no errors.  continue.
                self.myDB.connect(self.server)
                # set status comment for db update

                if (ticketNum):
                    jiraticketinfo = "ITSSUPP-" + str(ticketNum)
                else:
                    jiraticketinfo = ""
                self.sComment = jiraticketinfo + ' Project Manager ID updated to ' + pmID + ' by ITS Support Web Service(Support_updatePM.py)'
                self.spsOrFds = spFDs   # save the sp ids (or fd ids ) from the input

                print (function)
                if function=="UpdatePM":
                    self.functionText = "UpdatePM"
                    self.updatePM(pmID)
                    if (ticketNum):  # if jira ticket number was inputted
                        message = 'FDs =' + spFDs + ' and children have been abandoned by ITS Support Web Service'
                        message = 'Project Manager ID updated to ' + pmID +  ' for FDs =' + spFDs + ' and children by ITS Support Web Service'
                        self.myJira.addCommentToJira(ticketNum, message)
                        # add query string to comment for deleveloper to comfirm database change.
                        message = "Use this query to confirm the PM ID has been changed in the Database:\n" + self.confirmQuery
                        roleValue = "Developers"
                        self.myJira.addCommentToJira(ticketNum, message, roleValue)
                        self.myJira.transitionTicket(ticketNum, 'Resolve and Close')
                elif function =="UpdatePMtest":
                    self.functionText = "UpdatePMtest"
                    self.updatePM(pmID)
                else:
                    self.errorCnt = 1
                    self.errorMessage = "No such function!"

        print("\nerror count = ", self.errorCnt)


    # ----------------------------------
    # update PM   in  database tables of
    # FD, APs,  SPs,
    # ---------------------------------

    def updatePM(self,pmID):
        fdIds = self.getFDsToProcess()

        # get all sps associated with FD
        query = 'select sp.sequencing_project_id from uss.dt_final_deliv_project fd ' \
                'left join uss.dt_sequencing_project sp on fd.final_deliv_project_id = sp.final_deliv_project_id ' \
                'where fd.final_deliv_project_id in (' + str(fdIds) + ')'

        spList = self.myDB.doQueryGetAllRows(query)
        self.spSet = set(spList)
        print("SPS:")
        print(self.spSet)

        # get all aps associated with FD
        query = 'select ap.ANALYSIS_PROJECT_ID from uss.dt_final_deliv_project fd ' \
                'left join uss.DT_ANALYSIS_PROJECT ap on fd.final_deliv_project_id = ap.final_deliv_project_id ' \
                'where fd.final_deliv_project_id in (' + str(fdIds) + ')'
        apList = self.myDB.doQueryGetAllRows(query)
        self.apSet = set(apList)
        print("ANALYSIS PROJECTS:")
        print(self.apSet)

        # get all fds associated with FD
        query = 'select fd.final_deliv_project_id from uss.dt_final_deliv_project fd ' \
                'where fd.final_deliv_project_id in (' + str(fdIds) + ')'
        fdList = self.myDB.doQueryGetAllRows(query)
        self.fdSet = set(fdList)
        print("FINAL DELIVERABLE PROJECTS:")
        print(self.fdSet)
        print('\n')

        # update FD and children with new Program Manager ID
        if self.functionText == "UpdatePM":  # update database for  non-test function
            self.updateSPs(self.spSet, pmID)
            self.updateAPs(self.apSet, pmID)
            self.updateFDs(self.fdSet, pmID)

        query = ("select distinct "
                 "fd.final_deliv_project_id as fdid,  \n" \
                 "fd.DEFAULT_PROJECT_MANAGER_ID as fd_pm, \n "\
                 "fd.status_COMMENTS as fd_comments,  \n"\
                 "fd.LAST_UPDATED_BY, \n "\
                 "fd.LAST_UPDATED,  \n"\
                 "ap.ANALYSIS_PROJECT_ID as ap_id, \n "\
                 "ap.PROJECT_MANAGER_ID as ap_pm, \n "\
                 "ap.status_COMMENTS as ap_comments,  \n"\
                 "ap.LAST_UPDATED_BY,  \n"\
                 "ap.LAST_UPDATED, \n"\
                 "sp.sequencing_project_id as spid,  \n"\
                 "sp.SEQUENCING_PROJECT_MANAGER_ID as sp_pm, \n "\
                 "sp.status_COMMENTS as sp_comments, \n "\
                 "sp.LAST_UPDATED_BY,  \n"\
                 "sp.LAST_UPDATED  \n"\
                 "from uss.dt_final_deliv_project fd  \n"\
                 "left join uss.DT_ANALYSIS_PROJECT ap on fd.final_deliv_project_id = ap.final_deliv_project_id  \n"\
                 "left join uss.dt_sequencing_project sp on fd.final_deliv_project_id = sp.final_deliv_project_id  \n"\
                 "where  fd.FINAL_DELIV_PROJECT_ID in (" + fdIds + ");")
        self.confirmQuery = query


        #get program manager first and last name using the contacts ws

        requestURL = "https://contacts.jgi.doe.gov/api/contacts/" +pmID  #contact_id
        pmInfo = self.wsTool.runWS(requestURL)
        print("-------------------------------------------")
        name = pmInfo["last_name"] + "," + pmInfo["first_name"]
        print(name)

        self.projectManagerName = name   #set this for Web report
        #now update Clarity program UDFs with name and pmID
        # i.e # https://jgi-int.claritylims.com/api/v2/projects?name=1241901


        spid = str(spList[0])
        print ("Updating Clarity UDFs for " + spid)
        projectURLlist = []


        if self.functionText == "UpdatePM":  # only update if this is not test function
            for sp in self.spSet:
                print(int(sp))
                projectURL = self.updatePM_UDFs(sp,name,pmID)  #need to do for all  sps
                projectURLlist.append(projectURL)
            self.urlSet = projectURLlist  #set this for Web report

        print("-------------------------------------------")
        print(
            "\n\n---- use this query to confirm the that the project manager has been changed in USS.Dt database: ----")
        print(query)

        print("\n\n------------------------------------------------------- ")
        print("The Clarity Project UDFs have been updated with the correct ")
        print("'Sequencing Project Manager' and 'Sequencing Project Manager Id'")
        self.clarityProjectsURLSmessage = "Use this list of project URLs to confirm that the update took place: \n Clarity Project List:\n"
        print("Use this list of project URLs to confirm that the update took place:")
        print("Clarity Project List:")
        for url in projectURLlist:

            print(url)

    # -----------------------------------------------------
    #  update the clarity UDFs for project manager name and id
    #  input :  spid

    def updatePM_UDFs(self, spid,name, pmID):
            url = self.myUDF.setURLproject(spid,self.clarityURL)
            projURL = url
            if url:
                projURL = self.myUDF.connectToClarityAPIprojects(url)
                print("project URL = ", projURL)

                projUDFs = self.myUDF.getProjectUDFs(projURL)
                print(projUDFs)
                projUDFs["Sequencing Project Manager"] = name
                projUDFs["Sequencing Project Manager Id"] = pmID
                status = self.myUDF.updateArtifactUDF(projURL, "Sequencing Project Manager", name)
                if status ==200:
                    status = self.myUDF.updateArtifactUDF(projURL, "Sequencing Project Manager Id", pmID)
                    if status == 200:
                        print("---!!Project UDFS updated!!----")
                    else:
                        self.errorMessage += "Sequencing Project Manager ID UDF not found in Clarity API"
                        self.errorCnt += 1

                else:
                    self.errorMessage += " Sequencing Project Manager Name UDF not found in Clarity API"
                    self.errorCnt += 1

            else:
                print ("***ERROR - did not get project URL")
            return projURL



    # -----------------------------------------------------
    #  Will return the list of FDs associated with command
    #
    def getFDsToProcess(self):
        fdORsp = self.spsOrFds
        fdId = []
        fdCountquery = "select count(fd.final_deliv_project_id) from uss.dt_final_deliv_project fd where fd.final_deliv_project_id in (" + fdORsp + ")"
        fdCount = self.myDB.doQuery(fdCountquery)
        # print(fdCount)
        if (fdCount == '0'):  # the user input are not FDs, try sps
            spCountquery = "select count(sp.sequencing_project_id) from uss.dt_sequencing_project sp where sp.sequencing_project_id in (" + fdORsp + ")"
            spCount = self.myDB.doQuery(spCountquery)
            if int(spCount) > 0:  # sps were inputted
                query = "select (sp.final_deliv_project_id) from uss.dt_sequencing_project sp where sp.sequencing_project_id in (" + fdORsp + ")"
                fdIdList = self.myDB.doQueryGetAllRows(query)  # get the FDs associated with SPs
                fdId = ",".join(map(str, set(fdIdList)))
            else:
                print("***** no FDs or SPs found in DB ******")
                self.errorCnt += 1
                self.errorMessage = "***** no FDs or SPs found in DB ******"
        else:
            fdId = fdORsp
        return fdId

    # ---------------------------------------------------------------------------------
    # update the sps with new PM
    def updateSPs(self, spSet, pmID):
        print("-------------------------------------------")
        print("update sps:", spSet, "with new PM: ", pmID)
        y = ",".join(map(str, spSet))
        # set PM in sps
        query = "update uss.dt_sequencing_project  set SEQUENCING_PROJECT_MANAGER_ID =" + pmID + "," \
                "status_comments = '" + self.sComment + "',status_date = sysdate, " \
                "last_updated_by ='" + self.submittedBy + "',last_updated = sysdate " \
                "where sequencing_project_id in (" + y + ") " \
                "and SEQUENCING_PROJECT_MANAGER_ID not in (" + pmID + ")"
        print(query)
        self.myDB.doUpate(query)

    # ---------------------------------------------------------------------------------
    # update the aps with new PM info
    def updateAPs(self, apSet, pmID):
        print("-------------------------------------------")
        print("update aps:", apSet, "with new PM: ", pmID)
        y = ",".join(map(str, apSet))
        query = "update uss.DT_ANALYSIS_Project set PROJECT_MANAGER_ID =" + pmID + "," \
                "status_comments = '" + self.sComment + "',status_date = sysdate, " \
                "last_updated_by ='" + self.submittedBy + "',last_updated = sysdate " \
                "where ANALYSIS_PROJECT_ID in (" + y + ") " \
                "and PROJECT_MANAGER_ID not in (" + pmID + ")"

        print(query)
        self.myDB.doUpate(query)


    # ---------------------------------------------------------------------------------
    # update the FDS with new PM info
    def updateFDs(self, fdSet, pmID):
        print("-------------------------------------------")
        print("update fds:", fdSet, "with new PM: ", pmID)
        y = ",".join(map(str, fdSet))
        query = "update uss.dt_final_deliv_project set DEFAULT_PROJECT_MANAGER_ID =" + pmID + "," \
                "status_comments = '" + self.sComment + "',status_date = sysdate, " \
                "last_updated_by ='" + self.submittedBy + "',last_updated = sysdate " \
                "where FINAL_DELIV_PROJECT_ID in (" + y + ") " \
                "and DEFAULT_PROJECT_MANAGER_ID not in (" + pmID + ")"

        print(query)
        self.myDB.doUpate(query)





