#from wsTools import wsTools
#from util_udfTools import udfTools

from itssupp.tools.util_dbTools import dbTools
from itssupp.tools.util_jiraTools import jiraTools


#gets the samples, sows, sps, aps, ats  and FDs that are related to FD and abandons them.
# Also abandon FD
# user inputs on command line:
#      server  = prod or dev or int
#       itssupp jira ticket number
#       the fds or sps that need to be abandon and children.
#       if sps are given,the associated fds will also be abandoned
# will output, all decendents of FD.  The update commands to abandon these entities,
# the query to confirm status update,
# and the URL and sample list to use in routetoworkflow to remove samples from clarity  queues


class Support_abandonFDorSPandChildren:
    """  This class is used to abandon FD and children or SP and children or SP and specific AP"""
    def __init__(self):
        self.myDB = dbTools()
        self.myJira = jiraTools()
        self.errorCnt = 0
        self.errorMessage = ""
        self.sComment = ""
        self.server = ''
        self.route2workflowURL = "https://pps-int.jgi.doe.gov/route-to-workflow/"
        self.functionText = ""
        self.confirmQuery = ""
        self.spsOrFds = ""
        self.aps =""
        self.sampleSet = ""
        self.sowSet = ""
        self.spSet = ""
        self.apSet = ""
        self.atSet = ""
        self.fdSet = ""
        self.servername= ""

    #-----------------------------------------------------
    # get all children of related FDs  and abandon
    # inputs:  server -  "prod"  from production (pluss-genprd1),
    #                    'dev' for development (plus-dwint1)
    def doAbandon(self,server,ticketNum,function,spFDs,aps=""):
        """ main method to execute the abandonment of FDs or SPs"""
        print("in doAbandon")
        print (server,ticketNum,function,spFDs,aps)

        if server  not in ('Prod','Dev','Int'):
            self.errorCnt = 1
            self.errorMessage = "No such server!"
        else:
            self.server = server
            #set the url for routetoworkflow
            if self.server == 'Prod':
                self.route2workflowURL = "https://pps-prd.jgi.doe.gov/route-to-workflow/" #11/13/20
                self.servername = "jgi-prd"
            elif self.server == 'Dev':
                self.route2workflowURL = "https://pps-dev.jgi.doe.gov/route-to-workflow/"   #needs new url
                self.servername = "jgi-dev"
            elif self.server == 'Int':
                self.route2workflowURL = "https://pps-int.jgi.doe.gov/route-to-workflow/"  #11/13/20
                self.servername = "jgi-int"
            else:
                self.route2workflowURL = ""
                self.servername = ""
                self.errorCnt = 1
                self.errorMessage = "No such server!"

            if self.errorCnt==0:  # no errors.  continue.
                self.myDB.connect(self.server)
                self.myJira.connectToJira()
                # set status comment for db update
                # self.sComment = 'ITSSUPP-' + ticketNum + ' auto-updated by Support_updatePM.py'
                if ticketNum:
                    jiraticketinfo = "ITSSUPP-" + str(ticketNum)
                else:
                    jiraticketinfo = ""
                self.sComment = jiraticketinfo + \
                                '  auto-updated by ' \
                                'ITS Support Web Service(abandonFDorSPandChildren.py)'

                self.spsOrFds = spFDs   # save the sp ids (or fd ids ) from the input
                self.aps=aps
                andChildrenEOM = ' and children have been abandoned by ITS Support Web Service'
                print (function)
                if function=="FDchildren":
                    self.functionText = "abandonFDandChildren"
                    self.abandonFDandChildren()
                    if ticketNum:  #if jira ticket number was inputted
                        message = 'FDs/SPs =' + spFDs + andChildrenEOM
                        self.addCommentsAndClose(ticketNum, message)

                elif function =="SPchildren":
                    self.functionText = "abandonSPandChildren"
                    self.abandonSPandChildren()
                    if ticketNum:  # if jira ticket number was inputted
                        message = 'SPs =' + spFDs + andChildrenEOM
                        self.addCommentsAndClose(ticketNum,message)


                elif function == "APSPchildren":
                    self.functionText = "abandonAPandSPandChildren"
                    self.abandonAPandSPandChildren()
                    if ticketNum:  # if jira ticket number was inputted
                        message = 'SPs =' + spFDs + ' and APs =' + aps + andChildrenEOM
                        self.addCommentsAndClose(ticketNum, message)

                elif function == "FDchildrenTest":
                    print ("at FD Test")
                    self.functionText = "abandonFDandChildrenTest"
                    self.abandonFDandChildren()
                elif function == "SPchildrenTest":
                    self.functionText = "abandonSPandChildrenTest"
                    self.abandonSPandChildren()
                elif function == "APSPchildrenTest":
                    self.functionText = "abandonAPandSPandChildrenTest"
                    self.abandonAPandSPandChildren()
                else:
                    self.errorCnt = 1
                    self.errorMessage = "No such function!"

        print("error count=", self.errorCnt)

    # ----------------------------------
    # abandon  AP, SP and Children
    # FD, APs, ATs, SPs, Samples, sows
    # ---------------------------------

    def addCommentsAndClose(self, ticketNum,message):
        "calls Jira to add comments to the Jira ticket anc close the ticket"
        self.myJira.addCommentToJira(ticketNum, message)
        message = "Use this query to confirm the statuses:\n" + self.confirmQuery
        roleValue = "Developers"
        self.myJira.addCommentToJira(ticketNum, message, roleValue)
        #do this for now, until get the api for route to workflow,  must do by hand for now
        message = "To do:  check if any samples need to be removed from queues." \
                  "\nUse this set of samples:\n"
        for sampleName in self.sampleSet:
            message += sampleName + "\n"
        message += "with routeToWorkflow controller:\n" + str(self.route2workflowURL)
        self.myJira.addCommentToJira(ticketNum, message, roleValue)
        # will close automatically once the route to workflow is working automatically
       # self.myJira.transitionTicket(ticketNum, 'Resolve and Close')




    # ----------------------------------
    # abandon  AP, SP and Children
    # FD, APs, ATs, SPs, Samples, sows
    # ---------------------------------
    def abandonAPandSPandChildren(self):
        "special function to abandon specific SP and AP combinations"
        spId = self.getSPsToProcess()
        apId = self.getAPsToProcess()  # from user input
        if self.errorCnt == 0:  # no errors.  continue.

            # get all samples from DB
            query = 'select sam.SAMPLE_ID from uss.dt_sequencing_project sp ' \
                    'left join uss.dt_sow_item sow on sp.sequencing_project_id = ' \
                    'sow.sequencing_project_id ' \
                    'left join uss.dt_m2m_samplesowitem m2sam on sow.sow_item_id = ' \
                    'm2sam.sow_item_id ' \
                    'left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id ' \
                    'where sp.sequencing_project_id in (' + str(spId) + ')'
            # print(query)
            sampleList = self.myDB.doQueryGetAllRows(query)
            sampleSet = set(sampleList)
            self.sampleSet = sampleSet

            # get all sows
            query = 'select sow.SOW_ITEM_ID from uss.dt_sow_item sow ' \
                    'where sow.sequencing_project_id in (' + str(spId) + ')'
            # print (query)
            sowList = self.myDB.doQueryGetAllRows(query)
            sowSet = set(sowList)
            self.sowSet = sowSet

            # get all sps
            query = 'select sp.sequencing_project_id from uss.dt_sequencing_project sp ' \
                    'where sp.sequencing_project_id in (' + str(spId) + ')'
            spList = self.myDB.doQueryGetAllRows(query)
            spSet = set(spList)
            self.spSet = spSet

            # get all aps associated with SP

            query = 'select ap.ANALYSIS_PROJECT_ID from uss.DT_ANALYSIS_PROJECT ap ' \
                    'where ap.ANALYSIS_PROJECT_ID in (' + str(apId) + ')'
            apList = self.myDB.doQueryGetAllRows(query)
            apSet = set(apList)
            self.apSet = apSet

            # get all ats associated with APs
            query = 'select ANALYSIS_TASK_ID from uss.DT_ANALYSIS_TASK  ' \
                    'where ANALYSIS_PROJECT_ID in (' + str(apId) + ')'
            print(query)
            atList = self.myDB.doQueryGetAllRows(query)
            print (atList)
            atSet = set(atList)
            self.atSet = atSet

            print (self.functionText)

            # update database for  non-test function
            if self.functionText == "abandonAPandSPandChildren":
                self.abandonSamples(sampleSet)
                self.abandonSows(sowSet)
                self.abandonSPs(spSet)
                self.abandonAPs(apSet)
                self.cancelATs(atSet)

            query = ("select distinct "
                     "fd.final_deliv_project_id as fdid, "
                     "fdcv.status as fd_status, "
                     "ap.ANALYSIS_PROJECT_ID as ap_id, "
                     "apcv.status as ap_status, "
                     "at.ANALYSIS_TASK_ID as at_id, "
                     "atcv.status as at_status, "
                     "jamo.at_in_jamo, "
                     "sp.sequencing_project_id as spid, "
                     "spcv.STATUS as spstatus, "
                     "sp.SEQUENCING_PROJECT_NAME, "
                     "sam.SAMPLE_ID as samid, "
                     "samcv.STATUS as samstatus,"
                     "sow.SOW_ITEM_ID as sowid,"
                     "sowcv.STATUS as sowstatus "
                     "from uss.dt_final_deliv_project fd "
                     "left join uss.DT_ANALYSIS_PROJECT ap on "
                     "fd.final_deliv_project_id = ap.final_deliv_project_id "
                     "left join uss.DT_ANALYSIS_TASK at on  "
                     "ap.ANALYSIS_PROJECT_ID = at.ANALYSIS_PROJECT_ID "
                     "left join uss.dt_sequencing_project sp on "
                     "fd.final_deliv_project_id = sp.final_deliv_project_id "
                     "left join uss.dt_sow_item sow on "
                     "sp.sequencing_project_id = sow.sequencing_project_id "
                     "left join uss.dt_m2m_samplesowitem m2sam on "
                     "sow.sow_item_id = m2sam.sow_item_id "
                     "left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id "
                     "LEFT JOIN uss.dt_sow_item_status_cv sowcv ON "
                     "sowcv.STATUS_ID = sow.CURRENT_STATUS_ID "
                     "LEFT JOIN uss.dt_sample_status_cv samcv ON "
                     "samcv.STATUS_ID = sam.CURRENT_STATUS_ID "
                     "LEFT JOIN uss.DT_SEQ_PROJECT_STATUS_CV spcv ON "
                     "spcv.STATUS_ID = sp.CURRENT_STATUS_ID "
                     "LEFT JOIN uss.DT_FINAL_DELIV_PROJ_STATUS_CV fdcv ON "
                     "fdcv.STATUS_ID = fd.CURRENT_STATUS_ID "
                     "LEFT JOIN uss.DT_ANALYSIS_TASK_STATUS_CV atcv ON "
                     "atcv.STATUS_ID = at.CURRENT_STATUS_ID "
                     "LEFT JOIN uss.DT_ANALYSIS_PROJECT_STATUS_CV apcv ON "
                     "apcv.STATUS_ID = ap.CURRENT_STATUS_ID "
                     "LEFT JOIN uss.VW_ANALYSIS_PROJECTS_AND_TASKS jamo ON "
                     "at.ANALYSIS_task_ID = jamo.ANALYSIS_task_ID "
                     "where  sp.sequencing_project_id in (" + spId + ") ;")
            self.confirmQuery = query

    # ----------------------------------
    # abandon FD and Children
    # FD, APs, ATs, SPs, Samples, sows
    # ---------------------------------
    def abandonFDandChildren(self):
        "will abandon FD and related children"
        fdId = self.getFDsToProcess()
        if self.errorCnt == 0:  # no errors.  continue.
            # get all samples associated with FD and abandon
            query = 'select sam.SAMPLE_ID from uss.dt_final_deliv_project fd ' \
                    'left join uss.dt_sequencing_project sp on ' \
                    'fd.final_deliv_project_id = sp.final_deliv_project_id ' \
                    'left join uss.dt_sow_item sow on ' \
                    'sp.sequencing_project_id = sow.sequencing_project_id ' \
                    'left join uss.dt_m2m_samplesowitem m2sam on ' \
                    'sow.sow_item_id = m2sam.sow_item_id ' \
                    'left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id ' \
                    'where fd.final_deliv_project_id in (' + str(fdId) + ')'
            sampleList = self.myDB.doQueryGetAllRows(query)
            sampleSet = set(sampleList)
            self.sampleSet = sampleSet

            # get all sows associated with FD and abandon
            query = 'select sow.SOW_ITEM_ID from uss.dt_final_deliv_project fd ' \
                    'left join uss.dt_sequencing_project sp on ' \
                    'fd.final_deliv_project_id = sp.final_deliv_project_id ' \
                    'left join uss.dt_sow_item sow on ' \
                    'sp.sequencing_project_id = sow.sequencing_project_id ' \
                    'where fd.final_deliv_project_id in (' + str(fdId) + ')'

            sowList = self.myDB.doQueryGetAllRows(query)
            sowSet = set(sowList)
            self.sowSet = sowSet

            # get all sps associated with FD and abandon
            query = 'select sp.sequencing_project_id from uss.dt_final_deliv_project fd ' \
                    'left join uss.dt_sequencing_project sp on ' \
                    'fd.final_deliv_project_id = sp.final_deliv_project_id ' \
                    'where fd.final_deliv_project_id in (' + str(fdId) + ')'

            spList = self.myDB.doQueryGetAllRows(query)
            spSet = set(spList)
            self.spSet = spSet

            # get all aps associated with FD and abandon
            query = 'select ap.ANALYSIS_PROJECT_ID from uss.dt_final_deliv_project fd ' \
                    'left join uss.DT_ANALYSIS_PROJECT ap on ' \
                    'fd.final_deliv_project_id = ap.final_deliv_project_id ' \
                    'where fd.final_deliv_project_id in (' + str(fdId) + ')'
            apList = self.myDB.doQueryGetAllRows(query)
            apSet = set (apList)
            self.apSet = apSet

            # get all ats associated with FD and abandon
            query = 'select at.ANALYSIS_TASK_ID from uss.dt_final_deliv_project fd ' \
                    'left join uss.DT_ANALYSIS_PROJECT ap on ' \
                    'fd.final_deliv_project_id = ap.final_deliv_project_id ' \
                    'left join uss.DT_ANALYSIS_TASK at on  ' \
                    'ap.ANALYSIS_PROJECT_ID = at.ANALYSIS_PROJECT_ID ' \
                    'where fd.final_deliv_project_id in (' + str(fdId) + ')'

            atList = self.myDB.doQueryGetAllRows(query)
            atSet = set(atList)
            self.atSet = atSet


            # get all fds associated with FD and abandon
            query = 'select fd.final_deliv_project_id from uss.dt_final_deliv_project fd ' \
                    'where fd.final_deliv_project_id in (' + str(fdId) + ')'
            fdList = self.myDB.doQueryGetAllRows(query)
            fdSet = set(fdList)
            self.fdSet = fdSet

            if self.functionText == "abandonFDandChildren": #update database for non-test function
                self.abandonSamples(sampleSet)
                self.abandonSows(sowSet)
                self.abandonSPs(spSet)
                self.abandonAPs(apSet)
                self.cancelATs(atSet)
                self.abandonFDs(fdSet)

            query = ("select distinct "
                     "fd.final_deliv_project_id as fdid, "
                     "fdcv.status as fd_status, "
                     "ap.ANALYSIS_PROJECT_ID as ap_id, "
                     "apcv.status as ap_status, "
                     "at.ANALYSIS_TASK_ID as at_id, "
                     "atcv.status as at_status, "
                     "jamo.at_in_jamo, "
                     "sp.sequencing_project_id as spid, "
                     "spcv.STATUS as spstatus, "
                     "sp.SEQUENCING_PROJECT_NAME, "
                     "sam.SAMPLE_ID as samid, "
                     "samcv.STATUS as samstatus,"
                     "sow.SOW_ITEM_ID as sowid,"
                     "sowcv.STATUS as sowstatus "
                     "from uss.dt_final_deliv_project fd "
                     "left join uss.DT_ANALYSIS_PROJECT ap on "
                     "fd.final_deliv_project_id = ap.final_deliv_project_id "
                     "left join uss.DT_ANALYSIS_TASK at on  "
                     "ap.ANALYSIS_PROJECT_ID = at.ANALYSIS_PROJECT_ID "
                     "left join uss.dt_sequencing_project sp on "
                     "fd.final_deliv_project_id = sp.final_deliv_project_id "
                     "left join uss.dt_sow_item sow on "
                     "sp.sequencing_project_id = sow.sequencing_project_id "
                     "left join uss.dt_m2m_samplesowitem m2sam on "
                     "sow.sow_item_id = m2sam.sow_item_id "
                     "left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id "
                     "LEFT JOIN uss.dt_sow_item_status_cv sowcv ON "
                     "sowcv.STATUS_ID = sow.CURRENT_STATUS_ID "
                     "LEFT JOIN uss.dt_sample_status_cv samcv ON "
                     "samcv.STATUS_ID = sam.CURRENT_STATUS_ID "
                     "LEFT JOIN uss.DT_SEQ_PROJECT_STATUS_CV spcv ON "
                     "spcv.STATUS_ID = sp.CURRENT_STATUS_ID "
                     "LEFT JOIN uss.DT_FINAL_DELIV_PROJ_STATUS_CV "
                     "fdcv ON fdcv.STATUS_ID = fd.CURRENT_STATUS_ID "
                     "LEFT JOIN uss.DT_ANALYSIS_TASK_STATUS_CV atcv ON "
                     "atcv.STATUS_ID = at.CURRENT_STATUS_ID "
                     "LEFT JOIN uss.DT_ANALYSIS_PROJECT_STATUS_CV apcv ON "
                     "apcv.STATUS_ID = ap.CURRENT_STATUS_ID "
                     "LEFT JOIN uss.VW_ANALYSIS_PROJECTS_AND_TASKS jamo ON "
                     "at.ANALYSIS_task_ID = jamo.ANALYSIS_task_ID "
                     "where  fd.FINAL_DELIV_PROJECT_ID in (" + fdId + ");" )

            self.confirmQuery = query





    # -----------------------------------------------------
    # abandon SP and children
    # SPs, samples and sows only
    # ------------------------------------------------------
    def abandonSPandChildren(self):
        """will abandon SP and related children"""
        spId = self.getSPsToProcess()
        if self.errorCnt == 0:  # no errors.  continue.
            if spId:
                # get all samples from DB
                query = 'select sam.SAMPLE_ID from uss.dt_sequencing_project sp ' \
                        'left join uss.dt_sow_item sow on ' \
                        'sp.sequencing_project_id = sow.sequencing_project_id ' \
                        'left join uss.dt_m2m_samplesowitem m2sam on ' \
                        'sow.sow_item_id = m2sam.sow_item_id ' \
                        'left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id ' \
                        'where sp.sequencing_project_id in (' + str(spId) + ')'
                print(query)
                sampleList = self.myDB.doQueryGetAllRows(query)
                sampleSet = set(sampleList)
                self.sampleSet = sampleSet

                # get all sows
                query = 'select sow.SOW_ITEM_ID from uss.dt_sow_item sow ' \
                        'where sow.sequencing_project_id in (' + str(spId) + ')'
                # print (query)
                sowList = self.myDB.doQueryGetAllRows(query)
                sowSet = set(sowList)
                self.sowSet = sowSet

                # get all sps
                query = 'select sp.sequencing_project_id from uss.dt_sequencing_project sp ' \
                        'where sp.sequencing_project_id in (' + str(spId) + ')'
                spList = self.myDB.doQueryGetAllRows(query)
                spSet = set(spList)
                self.spSet = spSet

                #update database for non-test function
                if self.functionText == "abandonSPandChildren":
                    self.abandonSamples(sampleSet)
                    self.abandonSows(sowSet)
                    self.abandonSPs(spSet)

                query = ("select distinct "
                         "sp.sequencing_project_id as spid, "
                         "spcv.STATUS as spstatus, "
                         "sp.SEQUENCING_PROJECT_NAME, "
                         "sam.SAMPLE_ID as samid, "
                         "samcv.STATUS as samstatus,"
                         "sow.SOW_ITEM_ID as sowid,"
                         "sowcv.STATUS as sowstatus "
                         "from uss.dt_sequencing_project sp "
                         "left join uss.dt_sow_item sow on "
                         "sp.sequencing_project_id = sow.sequencing_project_id "
                         "left join uss.dt_m2m_samplesowitem m2sam on "
                         "sow.sow_item_id = m2sam.sow_item_id "
                         "left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id "
                         "LEFT JOIN uss.dt_sow_item_status_cv sowcv ON "
                         "sowcv.STATUS_ID = sow.CURRENT_STATUS_ID "
                         "LEFT JOIN uss.dt_sample_status_cv samcv ON "
                         "samcv.STATUS_ID = sam.CURRENT_STATUS_ID "
                         "LEFT JOIN uss.DT_SEQ_PROJECT_STATUS_CV spcv ON "
                         "spcv.STATUS_ID = sp.CURRENT_STATUS_ID "
                         "where  sp.sequencing_project_id in (" + spId + ");")
                self.confirmQuery = query

    #-----------------------------------------------------
    # get the input from user, can be either SPs  or FDs,
    # separated by comma.  Will return the list of FDs associated with command
    #
    def getFDsToProcess(self):
        """gets the FDs to process from database query"""
        fdORsp = self.spsOrFds
        fdId = []
        fdCountquery = "select count(fd.final_deliv_project_id) from " \
                       "uss.dt_final_deliv_project fd " \
                       "where fd.final_deliv_project_id in (" + fdORsp + ")"
        fdCount = self.myDB.doQuery(fdCountquery)
        if fdCount == '0':  # the user input are not FDs, try sps
            spCountquery = "select count(sp.sequencing_project_id) from " \
                           "uss.dt_sequencing_project sp " \
                           "where sp.sequencing_project_id in (" + fdORsp + ")"
            spCount = self.myDB.doQuery(spCountquery)
            if int(spCount) > 0:  # sps were inputted
                query = "select (sp.final_deliv_project_id) from " \
                        "uss.dt_sequencing_project sp " \
                        "where sp.sequencing_project_id in (" + fdORsp + ")"
                fdIdList = self.myDB.doQueryGetAllRows(query)  # get the FDs associated with SPs
                fdId = ",".join(map(str, set(fdIdList)))
            else:
                print("***** no FDs or SPs found in DB ******")
                self.errorCnt += 1
                self.errorMessage = "***** no FDs or SPs found in DB ******"

        else:
            fdId = fdORsp
        return fdId

    # -----------------------------------------------------
    # get the input from user, get the APS
    # separated by comma.  Will return the list of APs associated with command
    #

    def getAPsToProcess(self):
        """will get the APs from database and return list"""
        aps = self.aps
        apCountquery = "select count(analysis_project_id) from " \
                       "uss.dt_analysis_project " \
                       "where analysis_project_id in (" + aps + ")"
        apCount = self.myDB.doQuery(apCountquery)
        if apCount == '0':  # the user input are not APs
            print("***** no APs found in DB ******")
            self.errorCnt += 1
            self.errorMessage = "***** no APs found in DB ******"
            aps = ""
        return aps
    # -----------------------------------------------------
    # get the input from user,
    # separated by comma.  Will return the list of SPs associated with command
    #

    def getSPsToProcess(self):
        """ returns list of SPs to process"""
        spList = self.spsOrFds
        print (self.spsOrFds)
        spID = spList
        spCountquery = "select count(sp.sequencing_project_id) from " \
                       "uss.dt_sequencing_project sp " \
                       "where sp.sequencing_project_id in (" + spList + ")"
        print (spCountquery)
        spCount = self.myDB.doQuery(spCountquery)
        if int(spCount) <= 0:  # sps were inputted
            print("***** no SPs found in DB ******")
            self.errorCnt += 1
            self.errorMessage = "***** no SPs found in DB ******"
            spID = ""
        return spID



    #---------------------------------------------------------------------------------
    def abandonSamples(self, sampleSet):
        """ updates the database to set the samples status to abandon"""

        # abandon the samples
        print("-------------------------------------------")
        print("abandon samples:", sampleSet, "if not abandoned or deleted already")
        if 'None' in sampleSet :
            print("*** no samples to abandon")

        else:
            y = ",".join(map(str, sampleSet))
            query = "update uss.dt_sample set current_status_id=13, " \
                    "status_comments = '" + self.sComment + "',status_date = sysdate " \
                    "where sample_id in (" + y + ") and current_status_id not in (13,16)"
            print(query)
            self.myDB.doUpate(query)


    # ---------------------------------------------------------------------------------
    def abandonSows(self,sowSet):
        """ updates the database to set the sows' status to abandon"""
        # abandon the sows
        print("-------------------------------------------")
        print("abandon sows:", sowSet, "if not abandoned, completed or deleted already")
        if 'None' in sowSet :
            print("*** no sows to abandon")

        else:
            y = ",".join(map(str, sowSet))
            query = "update uss.dt_sow_item set current_status_id=7, " \
                    "status_comments = '" + self.sComment + "',status_date = sysdate " \
                    "where sow_item_id in (" + y + ") " \
                    "and current_status_id not in (6,7,10)"
            print(query)
            self.myDB.doUpate(query)

    # ---------------------------------------------------------------------------------
    def abandonSPs(self,spSet):
        """ updates the database to set the SPs' status to abandon"""
        # abandon the sps
        print("-------------------------------------------")
        print("abandon SPs:", spSet, "if not abandoned, completed or deleted already")
        if 'None' in spSet:
            print("*** no SPs to abandon")

        else:
            y = ",".join(map(str, spSet))
            query = "update uss.dt_sequencing_project  set current_status_id=7, " \
                    "status_comments = '" + self.sComment + "',status_date = sysdate " \
                    "where sequencing_project_id in (" + y + ") " \
                    "and current_status_id not in (7,9)"
            print(query)
            self.myDB.doUpate(query)

    # ---------------------------------------------------------------------------------
    # abandon the aps
    def abandonAPs(self,apSet):
        """ updates the database to set the APs' status to abandon"""
        print("-------------------------------------------")
        print("abandon APs:", apSet, "if not abandoned,completed or deleted already")
        if 'None' in apSet:
            print("*** no APs to abandon")

        else:
            y = ",".join(map(str, apSet))
            query = "update uss.DT_ANALYSIS_Project  set current_status_id=3, " \
                    "status_comments = '" + self.sComment + "',status_date = sysdate " \
                    "where ANALYSIS_PROJECT_ID in (" + y + ") " \
                    "and current_status_id not in (3,4)"
            print(query)
            self.myDB.doUpate(query)

    # ---------------------------------------------------------------------------------
    # cancel the ats
    def cancelATs(self,atSet):
        """ updates the database to set the ATs' status to cancel"""
        print("-------------------------------------------")
        print("cancel ats:", atSet, "if not cancelled,done or deleted already")
        if 'None' in atSet:
            print("*** no ATs to cancel")

        else:
            for at in atSet:  # cancel each AT individually, (only if not in jamo)
                query = "DECLARE " \
                "inJamo INTEGER; " \
                "BEGIN " \
                "select  COUNT(*) INTO inJamo from uss.DT_JAMO_DATA_ATS " \
                    "where ANALYSIS_TASK_ID in (" + at + "); " \
                    "update uss.DT_ANALYSIS_TASK  set current_status_id=10, " \
                    "status_comments = '" + self.sComment + "',status_date = sysdate " \
                    "where  ANALYSIS_TASK_ID in (" + at + ") " \
                    "and current_status_id not in (9,10,13) and inJamo = 0; " \
                "END;"

                print(query)
                self.myDB.doUpate(query)

    # -------------------------------------------------------------------
    # abandon the FDS
    #--------------------------------------------------------------------
    def abandonFDs(self,fdSet):
        """ updates the database to set the FDs' status to abandon"""
        print("-------------------------------------------")
        print("abandon fds:", fdSet, "if not abandoned,completed or deleted already")
        fdNames = ",".join(map(str, fdSet))
        query = "update uss.dt_final_deliv_project  set current_status_id=6, " \
                "status_comments = '" + self.sComment + "',status_date = sysdate " \
                "where FINAL_DELIV_PROJECT_ID in (" + fdNames + ") " \
                "and current_status_id not in (5,6,8)"
        print(query)
        self.myDB.doUpate(query)
